package com.getjavajob.training.morozovm.socialnetwork.common;

import javax.persistence.*;

@Entity
@Table(name = "user_photo", catalog = "heroku_e931b211664d42a")
public class UserPhoto {

    @Id
    private int id;
    @Lob
    private byte[] photo;
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private User user;

    public UserPhoto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
