package com.getjavajob.training.morozovm.socialnetwork.common;

public enum Status {
    ACTIVE,
    BANNED
}
