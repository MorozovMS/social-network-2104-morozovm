package com.getjavajob.training.morozovm.socialnetwork.common;

public enum Permission {

    USERS_ADMIN("users:admin"),
    USERS_DELETE("users:delete"),
    USERS_WRITE("users:write"),
    USERS_READONLY("users:readonly");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

}
