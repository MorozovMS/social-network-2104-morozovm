package com.getjavajob.training.morozovm.socialnetwork.common;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Role {

    ROLE_READONLY(Stream.of(Permission.USERS_READONLY).collect(Collectors.toCollection(HashSet::new))),
    ROLE_USER(Stream.of(Permission.USERS_WRITE).collect(Collectors.toCollection(HashSet::new))),
    ROLE_ADMIN(Stream.of(Permission.USERS_WRITE, Permission.USERS_DELETE, Permission.USERS_ADMIN)
            .collect(Collectors.toCollection(HashSet::new)));

    private final Set<Permission> permissions;

    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
    }

}
