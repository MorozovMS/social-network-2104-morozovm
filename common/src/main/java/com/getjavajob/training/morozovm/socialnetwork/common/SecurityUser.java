package com.getjavajob.training.morozovm.socialnetwork.common;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

public class SecurityUser implements UserDetails {

    private int id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private Set<SimpleGrantedAuthority> authorities;
    private boolean isActive;

    public SecurityUser() {
    }

    public SecurityUser(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.username = user.getEmail();
        this.password = user.getPassword();
        this.authorities = user.getRole().getAuthorities();
        this.isActive = user.getStatus().name().equalsIgnoreCase("active");
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isActive;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isActive;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }

    public int getId() {
        return id;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public boolean isAdmin() {
        return this.getAuthorities().stream()
                .anyMatch(permission -> permission.getAuthority().equalsIgnoreCase("users:admin"));
    }

}
