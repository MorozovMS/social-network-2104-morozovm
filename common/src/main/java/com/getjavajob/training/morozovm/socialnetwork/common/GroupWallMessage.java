package com.getjavajob.training.morozovm.socialnetwork.common;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "group_wall", catalog = "heroku_e931b211664d42a")
public class GroupWallMessage implements Comparable<GroupWallMessage> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;
    @ManyToOne
    @JoinColumn(name = "group_id", updatable = false)
    private Group group;
    private String message;
    @Column(insertable = false)
    private Date createdDate;

    public GroupWallMessage() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "GroupWallMessage{" +
                "id=" + id +
                ", creator=" + creator.getId() +
                ", group=" + group.getId() +
                ", message='" + message + '\'' +
                ", createdDate=" + createdDate +
                '}';
    }

    @Override
    public int compareTo(GroupWallMessage o) {
        return this.id - o.getId();
    }

}
