package com.getjavajob.training.morozovm.socialnetwork.common;

import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

@RedisHash
public class UserWallMessageDTO implements Serializable {

    private int id;
    private String message;
    private Date createdDate;
    private int userId;
    private int creatorId;
    private String creatorName;

    public UserWallMessageDTO() {

    }

    public UserWallMessageDTO(UserWallMessage message) {
        this.setId(message.getId());
        this.setMessage(message.getMessage());
        this.setCreatedDate(message.getCreatedDate());
        this.setUserId(message.getUser().getId());
        this.setCreatorId(message.getCreator().getId());
        this.setCreatorName(message.getCreator().getSurname() + " " + message.getCreator().getName());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserWallMessageDTO that = (UserWallMessageDTO) o;
        return id == that.id && userId == that.userId && creatorId == that.creatorId && Objects.equals(message, that.message) && createdDate.equals(that.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, message, createdDate, userId, creatorId);
    }

    @Override
    public String toString() {
        return "UserWallMessageDTO{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", createdDate=" + createdDate +
                ", userId=" + userId +
                ", creatorId=" + creatorId +
                '}';
    }

}
