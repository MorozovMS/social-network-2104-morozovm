package com.getjavajob.training.morozovm.socialnetwork.common;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@Entity
@Table(name = "phones", catalog = "heroku_e931b211664d42a")
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "phone")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @XmlElement(name = "phone")
    private String phone;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public Phone() {

    }

    public Phone(int id, String phone, User user) {
        this.id = id;
        this.phone = phone;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", user=" + user.getId() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phone phone1 = (Phone) o;
        return id == phone1.id && phone.equals(phone1.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone);
    }

}
