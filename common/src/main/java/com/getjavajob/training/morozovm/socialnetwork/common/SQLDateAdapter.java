package com.getjavajob.training.morozovm.socialnetwork.common;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;

public class SQLDateAdapter extends XmlAdapter<String, java.sql.Date> {

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public java.sql.Date unmarshal(String v) throws Exception {
        return new java.sql.Date(dateFormat.parse(v).getTime());
    }

    @Override
    public String marshal(java.sql.Date v) {
        return dateFormat.format(v);
    }

}
