package com.getjavajob.training.morozovm.socialnetwork.common;

import javax.persistence.*;

@Entity
@Table(name = "group_photo", catalog = "heroku_e931b211664d42a")
public class GroupPhoto {

    @Id
    private int id;
    @Lob
    private byte[] photo;
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Group group;

    public GroupPhoto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

}
