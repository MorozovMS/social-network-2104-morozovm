package com.getjavajob.training.morozovm.socialnetwork.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@JsonIgnoreProperties(value = {"members"})
@Entity
@Table(name = "groups", catalog = "heroku_e931b211664d42a")
public class Group implements Comparable<Group> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;
    @Column(updatable = false, insertable = false)
    private Date creationDate;
    @OneToOne
    @JoinColumn(name = "creator_id", updatable = false)
    private User creator;
    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
    private Set<User> members;

    public Group() {

    }

    public Group(int id, String name, String description, Date creationDate, User creator, Set<User> members) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
        this.creator = creator;
        this.members = members;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", creationDate=" + creationDate +
                ", creatorId=" + creator.getId() +
                ", members=" + members +
                '}';
    }

    @Override
    public int compareTo(Group o) {
        return this.getId() - o.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

}
