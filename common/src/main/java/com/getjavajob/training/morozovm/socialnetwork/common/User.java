package com.getjavajob.training.morozovm.socialnetwork.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@JsonIgnoreProperties(value = {"phones", "messages", "friends", "friendsRequests", "groups"})
@Entity
@Table(name = "users", catalog = "heroku_e931b211664d42a")
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Comparable<User> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String surname;
    private String midName;
    private String email;
    private String skype;
    private String telegram;
    @XmlTransient
    @Column(updatable = false)
    private String password;
    @Column(updatable = false, insertable = false)
    @XmlJavaTypeAdapter(SQLDateAdapter.class)
    private Date registerDate;
    @XmlJavaTypeAdapter(SQLDateAdapter.class)
    private Date bDay;
    private boolean sex;
    private String homeAddress;
    private String workAddress;
    private String addInfo;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_friends",
            joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "friend_id"))
    @XmlTransient
    private Set<User> friends;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_friends_requests",
            joinColumns = @JoinColumn(name = "sender_id"), inverseJoinColumns = @JoinColumn(name = "responder_id"))
    @XmlTransient
    private Set<User> friendsRequests;
    @XmlTransient
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_group",
            joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "group_id"))
    private Set<Group> groups;
    @XmlElementWrapper(name = "phones")
    @XmlElement(name = "phone")
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Phone> phones;
    @XmlTransient
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<UserWallMessage> messages;
    @Enumerated(value = EnumType.STRING)
    @Column(updatable = false)
    private Role role;
    @Enumerated(value = EnumType.STRING)
    @Column(updatable = false)
    private Status status;

    public User() {

    }

    private User(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.surname = builder.surname;
        this.midName = builder.midName;
        this.sex = builder.sex;
        this.email = builder.email;
        this.skype = builder.skype;
        this.telegram = builder.telegram;
        this.password = builder.password;
        this.bDay = builder.bDay;
        this.registerDate = builder.registerDate;
        this.homeAddress = builder.homeAddress;
        this.workAddress = builder.workAddress;
        this.addInfo = builder.addInfo;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", midName='" + midName + '\'' +
                ", email='" + email + '\'' +
                ", skype='" + skype + '\'' +
                ", telegram='" + telegram + '\'' +
                ", password='" + password + '\'' +
                ", registerDate=" + registerDate +
                ", bDay=" + bDay +
                ", sex=" + sex +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", addInfo='" + addInfo + '\'' +
                ", friends=" + friends +
                ", phones=" + phones +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return id == user.id && Objects.equals(name, user.name) && Objects.equals(surname, user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname);
    }

    @Override
    public int compareTo(User o) {
        return this.getId() - o.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public byte getSex() {
        if (sex) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public Date getBDay() {
        return bDay;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public Set<User> getFriends() {
        return friends;
    }

    public void setFriends(Set<User> friends) {
        this.friends = friends;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void setbDay(Date bDay) {
        this.bDay = bDay;
    }

    public List<UserWallMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<UserWallMessage> messages) {
        this.messages = messages;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<User> getFriendsRequests() {
        return friendsRequests;
    }

    public void setFriendsRequests(Set<User> friendsRequests) {
        this.friendsRequests = friendsRequests;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static class Builder {

        private final int id;
        private final String name;
        private final String surname;
        private final String email;
        private final String password;
        private final boolean sex;
        private Date registerDate;
        private String midName;
        private String skype;
        private String telegram;
        private Date bDay;
        private String homeAddress;
        private String workAddress;
        private String addInfo;

        public Builder(int id, String name, String surname, boolean sex, String email, String password) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.sex = sex;
            this.email = email;
            this.password = password;
        }

        public Builder setMidName(String midName) {
            this.midName = midName;
            return this;
        }

        public Builder setSkype(String skype) {
            this.skype = skype;
            return this;
        }

        public Builder setTelegram(String telegram) {
            this.telegram = telegram;
            return this;
        }

        public Builder setBDay(Date bDay) {
            this.bDay = bDay;
            return this;
        }

        public Builder setRegDate(Date regDay) {
            this.registerDate = regDay;
            return this;
        }

        public Builder setHomeAddress(String homeAddress) {
            this.homeAddress = homeAddress;
            return this;
        }

        public Builder setWorkAddress(String workAddress) {
            this.workAddress = workAddress;
            return this;
        }

        public Builder setAddInfo(String addInfo) {
            this.addInfo = addInfo;
            return this;
        }

        public User build() {
            return new User(this);
        }

    }

}
