package com.getjavajob.training.morozovm.socialnetwork.common;

public interface SearchResult {

    Integer getId();

    String getName();

    String getSurname();

    String getMidname();

    String getType();

}
