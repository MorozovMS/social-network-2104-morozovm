package com.getjavajob.training.morozovm.socialnetwork.common;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class UserDTO implements Serializable {

    private int userId;
    private String name;
    private String surname;
    private String midName;
    private String email;
    private Date bDay;
    private Timestamp eventDate;

    public UserDTO() {

    }

    public UserDTO(User user) {
        this.userId = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.midName = user.getMidName();
        this.email = user.getEmail();
        this.bDay = user.getBDay();
        this.eventDate = new Timestamp(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", midName='" + midName + '\'' +
                ", email='" + email + '\'' +
                ", bDay=" + bDay +
                ", eventDate=" + eventDate +
                '}';
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getbDay() {
        return bDay;
    }

    public void setbDay(Date bDay) {
        this.bDay = bDay;
    }

    public Timestamp getEventDate() {
        return eventDate;
    }

    public void setEventDate(Timestamp eventDate) {
        this.eventDate = eventDate;
    }

}
