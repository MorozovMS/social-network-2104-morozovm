package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.UserPhoto;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserPhotoDao;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhotoServiceTest {

    @Test
    public void checkPhotoExists() {
        UserPhotoDao userPhotoDao = mock(UserPhotoDao.class);
        when(userPhotoDao.existsById(1)).thenReturn(true);
        assertTrue(userPhotoDao.existsById(1));
    }

    @Test
    public void downloadPhoto() {
        UserPhotoDao userPhotoDao = mock(UserPhotoDao.class);
        UserPhoto userPhoto = new UserPhoto();
        userPhoto.setId(1);
        when(userPhotoDao.save(userPhoto)).thenReturn(userPhoto);
        assertEquals(1, userPhotoDao.save(userPhoto).getId());
    }

    @Test
    public void readPhoto() {
        UserPhotoDao userPhotoDao = mock(UserPhotoDao.class);
        when(userPhotoDao.getUserPhoto(1)).thenReturn(null);
        assertNull(userPhotoDao.getUserPhoto(1));
    }

}