package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Test
    public void createUser() {
        User testUser = new User.Builder(20, "name", "surname", true, "email", "pass").build();
        UserDao userDao = mock(UserDao.class);
        when(userDao.save(testUser)).thenReturn(testUser);
        assertEquals(userDao.save(testUser).getId(), testUser.getId());
    }

    @Test
    public void readUser() {
        User testUser = new User.Builder(20, "name", "surname", true, "email", "pass").build();
        UserDao userDao = mock(UserDao.class);
        when(userDao.findById(20)).thenReturn(testUser);
        assertEquals(20, userDao.findById(20).getId());
    }

}