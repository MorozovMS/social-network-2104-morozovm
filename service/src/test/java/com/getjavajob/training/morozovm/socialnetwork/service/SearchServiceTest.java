package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.dao.SearchDao;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchServiceTest {

    @Test
    public void search() {
        SearchDao searchDao = mock(SearchDao.class);
        when(searchDao.search("test", 0, 5)).thenReturn(new ArrayList<>());
        assertTrue(searchDao.search("test", 0, 5) instanceof ArrayList);
    }

    @Test
    public void rowCount() {
        SearchDao searchDao = mock(SearchDao.class);
        when(searchDao.getCount("test")).thenReturn(9);
        assertEquals(9, searchDao.getCount("test"));
    }

}