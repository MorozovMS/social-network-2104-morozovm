package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupDao;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GroupServiceTest {

    @Test
    public void saveGroup() {
        Group testGroup = new Group(1, "test", "test description", new Date(System.currentTimeMillis()), new User(), null);
        GroupDao groupDao = mock(GroupDao.class);
        when(groupDao.save(testGroup)).thenReturn(testGroup);
        assertEquals(groupDao.save(testGroup).getId(), testGroup.getId());
    }

    @Test
    public void readGroup() {
        Group testGroup = new Group(1, "test", "test description", new Date(System.currentTimeMillis()), new User(), null);
        GroupDao groupDao = mock(GroupDao.class);
        when(groupDao.findById(1)).thenReturn(testGroup);
        Group result = groupDao.findById(1);
        assertEquals(1, result.getId());
    }

}