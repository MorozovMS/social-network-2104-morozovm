package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatRoom;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.dao.ChatRoomDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class ChatRoomService {

    private final ChatRoomDao chatRoomDao;
    private final UserDao userDao;

    private static final int PAGE_SIZE = 5;

    @Autowired
    public ChatRoomService(ChatRoomDao chatRoomDao, UserDao userDao) {
        this.chatRoomDao = chatRoomDao;
        this.userDao = userDao;
    }

    public int getChatId(int senderId, int recipientId) {
        if (checkUserAccess(senderId)) {
            if (chatRoomDao.checkExistsBySenderIdAndRecipientId(senderId, recipientId) == 0) {
                ChatRoom chatRoom = new ChatRoom();
                chatRoom.setSender(userDao.findById(senderId));
                chatRoom.setRecipient(userDao.findById(recipientId));
                chatRoomDao.save(chatRoom);
            }
            return chatRoomDao.getChatId(senderId, recipientId);
        } else {
            return -1;
        }
    }

    public Page<ChatRoom> getChatRooms(int id, int page) {
        if (checkUserAccess(id)) {
            return chatRoomDao.getAllUsersChats(id, PageRequest.of(page, PAGE_SIZE));
        } else {
            return null;
        }
    }

    private boolean checkUserAccess(int id) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == id || securityUser.isAdmin();
    }

}
