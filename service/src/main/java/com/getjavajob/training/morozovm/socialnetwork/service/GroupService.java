package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.GroupWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupWallDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupService {

    private final GroupDao groupDao;
    private final UserDao userDao;
    private final GroupWallDao groupWallDao;

    private static final int PAGE_SIZE = 5;

    @Autowired
    public GroupService(GroupDao groupDao, UserDao userDao, GroupWallDao groupWallDao) {
        this.groupDao = groupDao;
        this.userDao = userDao;
        this.groupWallDao = groupWallDao;
    }

    @Transactional
    public int createGroup(Group group) {
        return groupDao.save(group).getId();
    }

    @Transactional
    public Group readGroup(int id) {
        return groupDao.findById(id);
    }

    @Transactional
    public boolean updateGroup(Group group) {
        if (checkUserAccess(group.getCreator().getId())) {
            groupDao.save(group);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean deleteGroup(Group group) {
        if (checkUserAccess(group.getCreator().getId())) {
            groupDao.delete(group);
            return true;
        } else {
            return false;
        }
    }

    public Page<Group> getAllGroups(int page) {
        return groupDao.findAll(PageRequest.of(page, PAGE_SIZE));
    }

    @Transactional
    @PreAuthorize("hasAuthority('users:write')")
    public void sendMessage(GroupWallMessage message) {
        groupWallDao.save(message);
    }

    @Transactional
    @PreAuthorize("hasAuthority('users:write')")
    public boolean updateMessage(GroupWallMessage message) {
        if (checkUserAccess(message)) {
            groupWallDao.save(message);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean deleteMessage(GroupWallMessage message) {
        if (checkUserAccess(message)) {
            groupWallDao.delete(message);
            return true;
        } else {
            return false;
        }
    }

    public List<GroupWallMessage> getAllMessages(Group group) {
        return groupWallDao.findByGroupIdOrderByIdDesc(group.getId());
    }

    @Transactional
    public Page<User> getGroupMembers(int groupId, int page) {
        return userDao.findByGroupsId(groupId, PageRequest.of(page, PAGE_SIZE));
    }

    @Transactional
    public boolean checkMembership(int idGroup, int idUser) {
        return groupDao.checkMembership(idGroup, idUser) == 1;
    }

    @Transactional
    public boolean leaveGroup(int groupId, int userId) {
        if (checkUserAccess(userId)) {
            userDao.leaveGroup(userId, groupId);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean joinGroup(int groupId, int userId) {
        if (checkUserAccess(userId)) {
            userDao.joinGroup(userId, groupId);
            return true;
        } else {
            return false;
        }
    }

    public int findGroupCreatorId(int idGroup) {
        return groupDao.findCreatorId(idGroup);
    }

    private boolean checkUserAccess(int id) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == id || securityUser.isAdmin();
    }

    private boolean checkUserAccess(GroupWallMessage message) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == message.getCreator().getId()
                || securityUser.getId() == findGroupCreatorId(message.getGroup().getId())
                || securityUser.isAdmin();
    }

}
