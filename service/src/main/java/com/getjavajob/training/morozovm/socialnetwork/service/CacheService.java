package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessageDTO;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserWallDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CacheService {

    private static final int CACHE_SIZE = 50;
    private final RedisTemplate redisTemplate;
    private final UserWallDao userWallDao;

    @Autowired
    public CacheService(RedisTemplate redisTemplate, UserWallDao userWallDao) {
        this.redisTemplate = redisTemplate;
        this.userWallDao = userWallDao;
    }

    public int getTotalPages(int id) {
        return userWallDao.findNews(id, PageRequest.of(0, 10)).getTotalPages();
    }

    public Set<UserWallMessageDTO> getElements(int id) {
        if (cacheSize(id) == 0) {
            downloadCache(id, 0);
            return redisTemplate.opsForZSet().range(id, 0, 9);
        } else {
            return getCachedElements(id, 1);
        }
    }

    public Set<UserWallMessageDTO> getCachedElements(int id, int page) {
        int start = --page * 10;
        if (start >= cacheSize(id)) {
            downloadCache(id, start / CACHE_SIZE);
        }
        return redisTemplate.opsForZSet().range(id, start, start + 10);
    }

    public void removeFromCache(int id, UserWallMessage message) {
        redisTemplate.opsForZSet().remove(id, new UserWallMessageDTO(message));
    }

    public void addToCache(int id, UserWallMessage message) {
        UserWallMessageDTO messageDTO = new UserWallMessageDTO(message);
        redisTemplate.opsForZSet().add(id, messageDTO, System.currentTimeMillis());
    }

    private long cacheSize(int id) {
        return redisTemplate.opsForZSet().size(id);
    }

    private Page<UserWallMessage> findNews(int id, int cachePage) {
        return userWallDao.findNews(id, PageRequest.of(cachePage, CACHE_SIZE));
    }

    private void downloadCache(int id, int cachePage) {
        for (UserWallMessage message : findNews(id, cachePage)) {
            UserWallMessageDTO messageDTO = new UserWallMessageDTO(message);
            redisTemplate.opsForZSet().add(id, messageDTO, messageDTO.getCreatedDate().getTime());
        }
    }

}