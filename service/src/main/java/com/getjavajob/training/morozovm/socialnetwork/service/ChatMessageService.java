package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.ChatMessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatMessageService {

    private final ChatMessageDao chatMessageDao;

    @Autowired
    public ChatMessageService(ChatMessageDao chatMessageDao) {
        this.chatMessageDao = chatMessageDao;
    }

    public ChatMessage save(ChatMessage chatMessage) {
        chatMessageDao.save(chatMessage);
        return chatMessage;
    }

    public List<ChatMessage> findChatMessages(int chatId) {
        return chatMessageDao.findByChatRoom_Id(chatId);
    }

}
