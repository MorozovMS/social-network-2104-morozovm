package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.GroupPhoto;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.UserPhoto;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupPhotoDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserPhotoDao;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;

@Service
public class PhotoService {

    private final UserPhotoDao userPhotoDao;
    private final GroupPhotoDao groupPhotoDao;
    private final UserDao userDao;
    private final GroupDao groupDao;

    private static final int MAX_PHOTO_SIZE = 65535;

    @Autowired
    public PhotoService(UserPhotoDao userPhotoDao, GroupPhotoDao groupPhotoDao,
                        UserDao userDao, GroupDao groupDao) {
        this.userPhotoDao = userPhotoDao;
        this.groupPhotoDao = groupPhotoDao;
        this.userDao = userDao;
        this.groupDao = groupDao;
    }

    public boolean checkUserPhotoExists(int id) {
        return userPhotoDao.existsById(id);
    }

    @Transactional
    public int downloadUserPhoto(int id, InputStream fileContent) {
        if (checkUserAccess(id)) {
            UserPhoto photo = new UserPhoto();
            try {
                photo.setPhoto(IOUtils.toByteArray(fileContent));
            } catch (IOException e) {
                e.printStackTrace();
            }
            photo.setUser(userDao.findById(id));
            return userPhotoDao.save(photo).getId();
        } else {
            return -1;
        }
    }

    public byte[] readUserPhoto(int id) {
        return userPhotoDao.getUserPhoto(id).getPhoto();
    }

    @Transactional
    public int updateUserPhoto(int id, InputStream newFileContent) {
        if (checkUserAccess(id)) {
            UserPhoto newPhoto = userPhotoDao.getUserPhoto(id);
            try {
                newPhoto.setPhoto(IOUtils.toByteArray(newFileContent));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return userPhotoDao.save(newPhoto).getId();
        } else {
            return -1;
        }
    }

    @Transactional
    public boolean deleteUserPhoto(int id) {
        if (checkUserAccess(id)) {
            userPhotoDao.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public boolean checkGroupPhotoExists(int id) {
        return groupPhotoDao.existsById(id);
    }

    @Transactional
    public int downloadGroupPhoto(int id, InputStream fileContent) {
        Group group = groupDao.findById(id);
        if (checkUserAccess(group.getCreator().getId())) {
            GroupPhoto photo = new GroupPhoto();
            try {
                photo.setPhoto(IOUtils.toByteArray(fileContent));
            } catch (IOException e) {
                e.printStackTrace();
            }
            photo.setGroup(group);
            return groupPhotoDao.save(photo).getId();
        } else {
            return -1;
        }
    }

    public byte[] readGroupPhoto(int id) {
        return groupPhotoDao.getGroupPhoto(id).getPhoto();
    }

    @Transactional
    public int updateGroupPhoto(int id, InputStream newFileContent) {
        GroupPhoto photo = groupPhotoDao.getGroupPhoto(id);
        if (checkUserAccess(photo.getGroup().getCreator().getId())) {
            try {
                photo.setPhoto(IOUtils.toByteArray(newFileContent));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return groupPhotoDao.save(photo).getId();
        } else {
            return -1;
        }
    }

    @Transactional
    public boolean deleteGroupPhoto(int id) {
        GroupPhoto photo = groupPhotoDao.getGroupPhoto(id);
        if (checkUserAccess(photo.getGroup().getCreator().getId())) {
            groupPhotoDao.delete(photo);
            return true;
        } else {
            return false;
        }
    }

    public boolean checkSizePhoto(long size) {
        return size < MAX_PHOTO_SIZE;
    }

    private boolean checkUserAccess(int id) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == id || securityUser.isAdmin();
    }

}
