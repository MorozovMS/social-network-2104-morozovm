package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.common.UserDTO;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.DeliveryMode;
import java.util.Objects;

@Service("securityUserDetailsService")
public class SecurityUserDetailsService implements UserDetailsService {

    private final UserDao userDao;
    private final RabbitTemplate rabbitTemplate;

    static final String qName = "soc-net-queue";
    static final String exchange = "soc-net-exchange";

    @Autowired
    public SecurityUserDetailsService(UserDao userDao, RabbitTemplate rabbitTemplate) {
        this.userDao = userDao;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDao.findByEmail(email);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException(email);
        } else {
            UserDTO userDTO = new UserDTO(user);
            rabbitTemplate.convertAndSend(exchange, qName, userDTO, m -> {
                m.getMessageProperties().setDeliveryMode(MessageDeliveryMode.fromInt(DeliveryMode.PERSISTENT));
                return m;
            });
            return new SecurityUser(user);
        }
    }

}
