package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.stereotype.Service;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Service
public class UsersXMLMapper {

    @Autowired
    private Marshaller marshaller;
    @Autowired
    private Unmarshaller unmarshaller;

    public void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    public void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    public void userToXML(User user, OutputStream outputStream) throws IOException {
        marshaller.marshal(user, new StreamResult(outputStream));
    }

    public User XMLToUser(InputStream inputStream) throws IOException {
        return (User) unmarshaller.unmarshal(new StreamSource(inputStream));
    }

}
