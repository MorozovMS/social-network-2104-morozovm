package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.*;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.PhoneDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserWallDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class UserService {

    private final UserDao userDao;
    private final UserWallDao userWallDao;
    private final PhoneDao phoneDao;
    private final GroupDao groupDao;
    private final PasswordEncoder passwordEncoder;

    private static final int PAGE_SIZE = 5;

    @Autowired
    public UserService(UserDao userDao, UserWallDao userWallDao, PhoneDao phoneDao, GroupDao groupDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.userWallDao = userWallDao;
        this.phoneDao = phoneDao;
        this.groupDao = groupDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public int createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.ROLE_USER);
        user.setStatus(Status.ACTIVE);
        for (Phone phone : user.getPhones()) {
            if (validatePhone(phone.getPhone())) {
                throw new IllegalArgumentException("wrong phone number");
            }
        }
        return userDao.save(user).getId();
    }

    @Transactional
    public User readUser(int id) {
        User user = userDao.findById(id);
        user.setPhones(getUserPhones(id));
        user.setMessages(getAllMessages(user));
        return user;
    }

    @Transactional
    public int updateUser(User user) {
        if (checkUserAccess(user.getId())) {
            User managedUser = userDao.findById(user.getId());
            user.setFriends(managedUser.getFriends());
            user.setFriendsRequests(managedUser.getFriendsRequests());
            user.setGroups(managedUser.getGroups());
            for (Phone phone : user.getPhones()) {
                if (validatePhone(phone.getPhone())) {
                    throw new IllegalArgumentException("wrong phone number");
                }
            }
            for (Phone p : managedUser.getPhones()) {
                if (!user.getPhones().contains(p)) {
                    phoneDao.delete(p);
                }
            }
            return userDao.save(user).getId();
        } else {
            return -1;
        }
    }

    @Transactional
    public boolean updatePassword(int id, String password) {
        if (checkUserAccess(id)) {
            userDao.changePassword(id, passwordEncoder.encode(password));
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean deleteUser(int id) {
        if (checkUserAccess(id)) {
            User delUser = userDao.findById(id);
            delUser.setGroups(null);
            delUser.setFriends(null);
            delUser.setFriendsRequests(null);
            userDao.save(delUser);
            userDao.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean deleteFriend(int id, int friendId) {
        if (checkUserAccess(id, friendId)) {
            userDao.deleteFriend(id, friendId);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean sendFriendRequest(int id, int friendId) {
        if (checkUserAccess(id)) {
            userDao.sendFriendRequest(id, friendId);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public Set<User> getAllFriendRequests(int id) {
        return checkUserAccess(id) ? userDao.findUserFriendRequest(id) : null;
    }

    @Transactional
    public Set<User> getAllInputFriendRequests(int id) {
        return checkUserAccess(id) ? userDao.findUserInputFriendRequests(id) : null;
    }

    @Transactional
    public boolean deleteFriendRequest(int id, int friendId) {
        if (checkUserAccess(id, friendId)) {
            userDao.deleteFriendRequest(id, friendId);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public boolean acceptFriendRequest(int id, int friendId) {
        if (checkUserAccess(id)) {
            userDao.addFriend(id, friendId);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public Page<User> getAllFriends(int id, int page) {
        return userDao.findUserFriends(id, PageRequest.of(page, PAGE_SIZE));
    }

    @Transactional
    public Page<User> getAllUsers(int page) {
        return userDao.findAll(PageRequest.of(page, PAGE_SIZE));
    }

    @Transactional
    @PreAuthorize("hasAuthority('users:write')")
    public void sendMessage(UserWallMessage message) {
        userWallDao.save(message);
    }

    @Transactional
    public boolean deleteMessage(UserWallMessage message) {
        if (checkUserAccess(message)) {
            userWallDao.delete(message);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    @PreAuthorize("hasAuthority('users:write')")
    public UserWallMessage updateMessage(UserWallMessage message) {
        if (checkUserAccess(message)) {
            return userWallDao.save(message);
        } else {
            return null;
        }
    }

    public List<UserWallMessage> getAllMessages(User user) {
        return userWallDao.findByUserIdOrderByIdDesc(user.getId());
    }

    public List<Phone> getUserPhones(int id) {
        return phoneDao.getPhones(id);
    }

    @Transactional
    public Page<Group> getUserGroups(int id, int page) {
        return groupDao.getUserGroups(id, PageRequest.of(page, PAGE_SIZE));
    }

    public boolean checkFriendship(int id, int friendId) {
        return userDao.checkFriendship(id, friendId) == 1;
    }

    public boolean checkFriendshipRequest(int id, int friendId) {
        return userDao.checkFriendshipRequest(id, friendId) == 1;
    }

    public boolean isEmailExists(String email) {
        return userDao.existsByEmail(email);
    }

    public boolean isExistsById(int id) {
        return userDao.existsById(id);
    }

    @Transactional
    public int setReadOnlyMode(int id) {
        return userDao.setReadOnlyMode(id);
    }

    @Transactional
    public int removeReadOnlyMode(int id) {
        return userDao.removeReadOnlyMode(id);
    }

    @Transactional
    public void banUser(int id) {
        userDao.banUser(id);
    }

    @Transactional
    public void unbanUser(int id) {
        userDao.unbanUser(id);
    }

    public String getNameById(int id) {
        return userDao.getUserNameById(id);
    }

    private boolean validatePhone(String phone) {
        return !phone.matches("^\\d{9,12}$");
    }

    private boolean checkUserAccess(int id) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == id || securityUser.isAdmin();
    }

    private boolean checkUserAccess(int id, int friendId) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == id || securityUser.getId() == friendId || securityUser.isAdmin();
    }

    private boolean checkUserAccess(UserWallMessage message) {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return securityUser.getId() == message.getUser().getId() || securityUser.getId() == message.getCreator().getId()
                || securityUser.isAdmin();
    }

}
