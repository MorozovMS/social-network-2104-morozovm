package com.getjavajob.training.morozovm.socialnetwork.service;

import com.getjavajob.training.morozovm.socialnetwork.common.SearchResult;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class SearchService {

    private final UserDao userDao;

    private static final int PAGE_SIZE = 5;

    @Autowired
    public SearchService(UserDao userDao) {
        this.userDao = userDao;
    }

    public Page<SearchResult> search(String searchQuery, int page) {
        String search;
        if (searchQuery.split(" ").length > 1) {
            search = "%" + searchQuery.split(" ")[0] + "%";
            Page<SearchResult> result = userDao.search(search, PageRequest.of(page, PAGE_SIZE));
            if (result.getTotalElements() == 0) {
                search = "%" + searchQuery.split(" ")[1] + "%";
                return userDao.search(search, PageRequest.of(page, PAGE_SIZE));
            } else {
                return result;
            }
        } else {
            search = "%" + searchQuery + "%";
            return userDao.search(search, PageRequest.of(page, PAGE_SIZE));
        }
    }

}