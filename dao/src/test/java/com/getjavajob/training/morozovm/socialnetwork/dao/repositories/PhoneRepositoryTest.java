package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Phone;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class PhoneRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhoneRepository phoneRepository;

    @Test
    public void getPhones() {
        User user = new User();
        Phone phone = new Phone();
        phone.setUser(user);
        user.setPhones(Stream.of(phone).collect(Collectors.toList()));
        userRepository.save(user);
        assertEquals(1, phoneRepository.getPhones(user.getId()).size());
    }

}