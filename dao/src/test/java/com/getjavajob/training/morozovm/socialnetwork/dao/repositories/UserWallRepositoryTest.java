package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class UserWallRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserWallRepository userWallRepository;

    @Test
    public void findByUserIdOrderByIdDesc() {
        User user = new User();
        userRepository.save(user);
        UserWallMessage userWallMessage = new UserWallMessage();
        userWallMessage.setUser(user);
        userWallRepository.save(userWallMessage);
        assertEquals(1, userWallRepository.findByUserIdOrderByIdDesc(user.getId()).size());
    }

}