package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.GroupPhoto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class GroupPhotoRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupPhotoRepository groupPhotoRepository;

    @Test
    public void getGroupPhoto() {
        Group group = new Group();
        GroupPhoto groupPhoto = new GroupPhoto();
        groupPhoto.setGroup(group);
        groupPhotoRepository.save(groupPhoto);
        groupRepository.save(group);
        assertNotNull(groupPhotoRepository.getGroupPhoto(group.getId()));
    }

}