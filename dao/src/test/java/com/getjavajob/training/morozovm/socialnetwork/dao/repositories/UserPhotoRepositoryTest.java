package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.common.UserPhoto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class UserPhotoRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserPhotoRepository userPhotoRepository;

    @Test
    public void getUserPhoto() {
        User user = new User();
        userRepository.save(user);
        UserPhoto userPhoto = new UserPhoto();
        userPhoto.setUser(user);
        userPhotoRepository.save(userPhoto);
        assertNotNull(userPhotoRepository.getUserPhoto(user.getId()));
    }

}