package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GroupRepository groupRepository;

    @Test
    public void deleteFriend() {
        User user = new User();
        User friend1 = new User();
        User friend2 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        userRepository.save(friend2);
        userRepository.addFriend(user.getId(), friend1.getId());
        userRepository.addFriend(user.getId(), friend2.getId());
        long oldCount = userRepository.findUserFriends(user.getId(), PageRequest.of(0, 5)).getTotalElements();
        userRepository.deleteFriend(user.getId(), friend1.getId());
        long newCount = userRepository.findUserFriends(user.getId(), PageRequest.of(0, 5)).getTotalElements();
        assertEquals(oldCount, newCount + 1);
    }

    @Test
    public void addFriend() {
        User user = new User();
        User friend1 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        long oldCount = userRepository.findUserFriends(user.getId(), PageRequest.of(0, 5)).getTotalElements();
        userRepository.addFriend(user.getId(), friend1.getId());
        long newCount = userRepository.findUserFriends(user.getId(), PageRequest.of(0, 5)).getTotalElements();
        assertEquals(oldCount + 1, newCount);
    }

    @Test
    public void sendFriendRequest() {
        User user = new User();
        User friend1 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        int oldCount = userRepository.findUserInputFriendRequests(friend1.getId()).size();
        userRepository.sendFriendRequest(user.getId(), friend1.getId());
        int newCount = userRepository.findUserInputFriendRequests(friend1.getId()).size();
        assertEquals(oldCount + 1, newCount);
    }

    @Test
    public void findUserFriends() {
        User user = new User();
        User friend1 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        userRepository.addFriend(user.getId(), friend1.getId());
        List<User> friends = userRepository.findUserFriends(user.getId(), PageRequest.of(0, 5)).getContent();
        assertNotNull(friends.get(0));
        assertEquals(1, friends.size());
    }

    @Test
    public void findUserFriendRequest() {
        User user = new User();
        User friend1 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        int oldCount = userRepository.findUserFriendRequest(user.getId()).size();
        userRepository.sendFriendRequest(user.getId(), friend1.getId());
        Set<User> requests = userRepository.findUserFriendRequest(user.getId());
        assertNotNull(requests);
        assertEquals(oldCount + 1, requests.size());
    }

    @Test
    public void findUserInputFriendRequests() {
        User user = new User();
        User friend1 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        int oldCount = userRepository.findUserInputFriendRequests(friend1.getId()).size();
        userRepository.sendFriendRequest(user.getId(), friend1.getId());
        Set<User> requests = userRepository.findUserInputFriendRequests(friend1.getId());
        assertNotNull(requests);
        assertEquals(oldCount + 1, requests.size());
    }

    @Test
    public void deleteFriendRequest() {
        User user = new User();
        User friend1 = new User();
        userRepository.save(user);
        userRepository.save(friend1);
        userRepository.sendFriendRequest(user.getId(), friend1.getId());
        int oldCount = userRepository.findUserInputFriendRequests(friend1.getId()).size();
        userRepository.deleteFriendRequest(user.getId(), friend1.getId());
        int newCount = userRepository.findUserInputFriendRequests(friend1.getId()).size();
        assertEquals(oldCount - 1, newCount);
    }

    @Test
    public void leaveGroup() {
        User user = new User();
        userRepository.save(user);
        Group group = new Group();
        groupRepository.save(group);
        userRepository.joinGroup(user.getId(), group.getId());
        long oldCount = userRepository.findByGroupsId(group.getId(), PageRequest.of(0, 5)).getTotalElements();
        userRepository.leaveGroup(user.getId(), group.getId());
        long newCount = userRepository.findByGroupsId(group.getId(), PageRequest.of(0, 5)).getTotalElements();
        assertEquals(oldCount, newCount + 1);
    }

    @Test
    public void joinGroup() {
        User user = new User();
        userRepository.save(user);
        Group group = new Group();
        groupRepository.save(group);
        long oldCount = userRepository.findByGroupsId(group.getId(), PageRequest.of(0, 5)).getTotalElements();
        userRepository.joinGroup(user.getId(), group.getId());
        long newCount = userRepository.findByGroupsId(group.getId(), PageRequest.of(0, 5)).getTotalElements();
        assertEquals(oldCount, newCount - 1);
    }

    @Test
    public void setReadOnlyMode() {
        assertEquals(0, userRepository.setReadOnlyMode(999));
        User user = new User();
        userRepository.save(user);
        assertEquals(1, userRepository.setReadOnlyMode(user.getId()));
    }

    @Test
    public void removeReadOnlyMode() {
        assertEquals(0, userRepository.removeReadOnlyMode(999));
        User user = new User();
        userRepository.save(user);
        userRepository.setReadOnlyMode(user.getId());
        assertEquals(1, userRepository.removeReadOnlyMode(user.getId()));
    }

    @Test
    public void getUserNameById() {
        User user = new User();
        user.setName("test");
        userRepository.save(user);
        assertEquals("test", userRepository.getUserNameById(user.getId()));
    }

}