package com.getjavajob.training.morozovm.socialnetwork.dao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootApplication
@ComponentScan(basePackages = {"com.getjavajob.training.morozovm.socialnetwork"})
@EntityScan("com.getjavajob.training.morozovm.socialnetwork")
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

}