package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class GroupRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private UserRepository userRepository;

    private static final int PAGE_SIZE = 5;

    @Test
    public void getUserGroups() {
        User user = new User();
        Group group = new Group();
        groupRepository.save(group);
        user.setGroups(Stream.of(group).collect(Collectors.toSet()));
        userRepository.save(user);
        assertEquals(1, groupRepository.getUserGroups(user.getId(), PageRequest.of(0, PAGE_SIZE)).getTotalElements());
    }

    @Test
    public void findCreatorId() {
        User user = new User();
        userRepository.save(user);
        Group group = new Group();
        group.setCreator(user);
        groupRepository.save(group);
        assertEquals(user.getId(), groupRepository.findCreatorId(group.getId()));
    }

}