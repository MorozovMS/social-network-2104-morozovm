package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.GroupWallMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@DataJpaTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
public class GroupWallRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupWallRepository groupWallRepository;

    @Test
    public void findByGroupIdOrderByIdDesc() {
        Group group = new Group();
        groupRepository.save(group);
        GroupWallMessage groupWallMessage = new GroupWallMessage();
        groupWallMessage.setGroup(group);
        groupWallRepository.save(groupWallMessage);
        assertEquals(1, groupWallRepository.findByGroupIdOrderByIdDesc(group.getId()).size());
    }

}