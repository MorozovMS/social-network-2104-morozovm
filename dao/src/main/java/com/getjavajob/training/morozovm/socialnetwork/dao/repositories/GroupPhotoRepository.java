package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.GroupPhoto;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupPhotoDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

@Primary
@RepositoryDefinition(domainClass = GroupPhoto.class, idClass = Integer.class)
public interface GroupPhotoRepository extends GroupPhotoDao {

    @Query(value = "select g from GroupPhoto g where g.group.id = :id")
    GroupPhoto getGroupPhoto(@Param("id") int id);

}
