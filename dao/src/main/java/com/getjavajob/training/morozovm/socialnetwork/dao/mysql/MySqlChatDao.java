package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.ChatMessageDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MySqlChatDao implements ChatMessageDao {

    @Override
    public List<ChatMessage> findByChatRoom_Id(int chatId) {
        return null;
    }

    @Override
    public void save(ChatMessage chatMessage) {

    }

    @Override
    public void deleteById(int id) {

    }

}
