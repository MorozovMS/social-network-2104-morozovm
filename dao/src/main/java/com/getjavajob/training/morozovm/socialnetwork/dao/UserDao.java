package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.SearchResult;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface UserDao {

    User save(User user);

    Page<User> findByGroupsId(int groupId, Pageable pageable);

    Page<User> findAll(Pageable pageable);

    boolean existsByEmail(String email);

    boolean existsById(int id);

    User findByEmail(String email);

    User findById(int id);

    void deleteFriend(int id, int friendId);

    void addFriend(int id, int friendId);

    void sendFriendRequest(int id, int friendId);

    Page<User> findUserFriends(int id, Pageable pageable);

    Set<User> findUserFriendRequest(int id);

    Set<User> findUserInputFriendRequests(int id);

    void deleteFriendRequest(int id, int friendId);

    void changePassword(int id, String password);

    void deleteById(int id);

    int checkFriendship(int id, int friend_id);

    int checkFriendshipRequest(int id, int friend_id);

    void leaveGroup(int id, int idGroup);

    void joinGroup(int id, int idGroup);

    Page<SearchResult> search(String searchQuery, Pageable pageable);

    int setReadOnlyMode(int id);

    int removeReadOnlyMode(int id);

    void banUser(int id);

    void unbanUser(int id);

    String getUserNameById(int id);

}
