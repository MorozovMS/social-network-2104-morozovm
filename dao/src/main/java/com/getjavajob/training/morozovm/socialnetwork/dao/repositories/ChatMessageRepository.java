package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.ChatMessageDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.RepositoryDefinition;

import java.util.List;

@Primary
@RepositoryDefinition(domainClass = ChatMessage.class, idClass = Integer.class)
public interface ChatMessageRepository extends ChatMessageDao {

    List<ChatMessage> findByChatRoom_Id(long chatId);

}
