package com.getjavajob.training.morozovm.socialnetwork.dao;

import java.util.List;

public interface SearchDao {

    List<Object> search(String searchQuery, int offset, int limit);

    int getCount(String searchQuery);

}
