package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.Phone;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.PhoneDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MySqlGroupPhoneDao implements PhoneDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MySqlGroupPhoneDao() {

    }

    @Override
    public List<Phone> getPhones(int id) {
        try {
            return entityManager.createQuery("SELECT u from User u JOIN fetch u.phones where u.id = :id", User.class)
                    .setParameter("id", id).getSingleResult().getPhones();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void delete(Phone phone) {
        entityManager.remove(phone);
    }

}
