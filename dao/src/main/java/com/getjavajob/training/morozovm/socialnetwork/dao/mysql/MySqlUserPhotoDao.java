package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.UserPhoto;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserPhotoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.io.InputStream;
import java.sql.ResultSet;

@Repository
public class MySqlUserPhotoDao implements UserPhotoDao {

    private static final String CHECK_EXISTS = "Select user_id from user_photo where user_id = ?";
    private static final String DOWNLOAD_PHOTO = "INSERT INTO user_photo (user_id, photo) VALUES (?, ?)";
    private static final String READ_PHOTO = "Select photo from user_photo where user_id = ?";
    private static final String UPDATE_PHOTO = "UPDATE user_photo SET photo = ? WHERE user_id = ?";
    private static final String DELETE_PHOTO = "DELETE from user_photo WHERE user_id = ?";

    private JdbcTemplate jdbcTemplate;

    public MySqlUserPhotoDao() {

    }

    @Autowired
    public MySqlUserPhotoDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public boolean existsById(int id) {
        return jdbcTemplate.query(CHECK_EXISTS, new Object[]{id}, ResultSet::next);
    }

    @Override
    public UserPhoto save(UserPhoto userPhoto) {
        jdbcTemplate.update(DOWNLOAD_PHOTO, userPhoto.getUser().getId(), userPhoto.getPhoto());
        return userPhoto;
    }

    public int updatePhoto(int id, InputStream newFileContent) {
        return jdbcTemplate.update(UPDATE_PHOTO, newFileContent, id);
    }

    @Override
    public UserPhoto getUserPhoto(int id) {
        UserPhoto photo = new UserPhoto();
        photo.setPhoto(jdbcTemplate.queryForObject(READ_PHOTO, new Object[]{id}, (resultSet, i) -> resultSet.getBytes(1)));
        photo.setId(id);
        return photo;
    }

    @Override
    public void deleteById(int id) {
        jdbcTemplate.update(DELETE_PHOTO, id);
    }

}
