package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.UserPhoto;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserPhotoDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

@Primary
@RepositoryDefinition(domainClass = UserPhoto.class, idClass = Integer.class)
public interface UserPhotoRepository extends UserPhotoDao {

    @Query("select u from UserPhoto u where u.id = :id")
    UserPhoto getUserPhoto(@Param("id") int id);

    @Modifying
    @Query(value = "delete from user_photo where user_id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);

}
