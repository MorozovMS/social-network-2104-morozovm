package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.User;

import java.util.List;

public interface UserFriendsDao {

    List<Integer> getFriends(User user);

    int becomeFriends(User friend1, User friend2);

    int deleteFriend(User user, User lostFriend);

    int updateFriend(User user, User oldFriend, User newFriend);

}
