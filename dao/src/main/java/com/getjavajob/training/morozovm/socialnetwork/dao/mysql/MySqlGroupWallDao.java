package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.GroupWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupWallDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class MySqlGroupWallDao implements GroupWallDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MySqlGroupWallDao() {

    }

    @Override
    public GroupWallMessage save(GroupWallMessage message) {
        entityManager.persist(message);
        return message;
    }

    @Override
    public void delete(GroupWallMessage message) {
        Object managed = entityManager.merge(message);
        entityManager.remove(managed);
    }

    public void updateMessage(GroupWallMessage newMessage) {
        entityManager.merge(newMessage);
    }

    @Override
    public List<GroupWallMessage> findByGroupIdOrderByIdDesc(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupWallMessage> criteriaQuery = criteriaBuilder.createQuery(GroupWallMessage.class);
        Root<GroupWallMessage> from = criteriaQuery.from(GroupWallMessage.class);
        criteriaQuery.select(from);
        criteriaQuery.where(criteriaBuilder.equal(from.get("groupId"), id));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

}
