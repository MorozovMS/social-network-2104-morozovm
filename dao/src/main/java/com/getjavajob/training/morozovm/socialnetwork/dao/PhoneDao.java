package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.Phone;

import java.util.List;

public interface PhoneDao {

    List<Phone> getPhones(int id);

    void delete(Phone phone);

}
