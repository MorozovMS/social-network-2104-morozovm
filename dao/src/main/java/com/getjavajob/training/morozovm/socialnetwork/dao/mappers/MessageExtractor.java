package com.getjavajob.training.morozovm.socialnetwork.dao.mappers;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class MessageExtractor implements ResultSetExtractor<Map<Integer, String>> {

    @Override
    public Map<Integer, String> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        Map<Integer, String> mapRet = new TreeMap<>(Collections.reverseOrder());
        while (resultSet.next()) {
            mapRet.put(resultSet.getInt("rowId"), resultSet.getString("message"));
        }
        return mapRet;
    }

}
