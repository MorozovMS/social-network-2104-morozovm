package com.getjavajob.training.morozovm.socialnetwork.dao.mappers;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserGroupsExtractor implements ResultSetExtractor<List<Integer>> {

    @Override
    public List<Integer> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<Integer> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getInt(1));
        }
        return result;
    }

}
