package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.GroupPhoto;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupPhotoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.io.InputStream;
import java.sql.ResultSet;

@Repository
public class MySqlGroupPhotoDao implements GroupPhotoDao {

    private static final String CHECK_EXISTS = "Select group_id from group_photo where group_id = ?";
    private static final String DOWNLOAD_PHOTO = "INSERT INTO group_photo (group_id, photo) VALUES (?, ?)";
    private static final String READ_PHOTO = "Select photo from group_photo where group_id = ?";
    private static final String UPDATE_PHOTO = "UPDATE group_photo SET photo = ? WHERE group_id = ?";
    private static final String DELETE_PHOTO = "DELETE from group_photo WHERE group_id = ?";

    private JdbcTemplate jdbcTemplate;

    public MySqlGroupPhotoDao() {

    }

    @Autowired
    public MySqlGroupPhotoDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public boolean existsById(int id) {
        return jdbcTemplate.query(CHECK_EXISTS, new Object[]{id}, ResultSet::next);
    }

    @Override
    public GroupPhoto save(GroupPhoto groupPhoto) {
        jdbcTemplate.update(DOWNLOAD_PHOTO, groupPhoto.getGroup().getId(), groupPhoto.getPhoto());
        return groupPhoto;
    }

    @Override
    public GroupPhoto getGroupPhoto(int id) {
        GroupPhoto groupPhoto = new GroupPhoto();
        groupPhoto.setPhoto(jdbcTemplate.queryForObject(READ_PHOTO, new Object[]{id}, (resultSet, i) -> resultSet.getBytes(1)));
        groupPhoto.setId(id);
        return groupPhoto;
    }

    public int updatePhoto(int id, InputStream newFileContent) {
        return jdbcTemplate.update(UPDATE_PHOTO, newFileContent, id);
    }

    @Override
    public void deleteById(int id) {
        jdbcTemplate.update(DELETE_PHOTO, id);
    }

    @Override
    public void delete(GroupPhoto groupPhoto) {
        deleteById(groupPhoto.getId());
    }

}
