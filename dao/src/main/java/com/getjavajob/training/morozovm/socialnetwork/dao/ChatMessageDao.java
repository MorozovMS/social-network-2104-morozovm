package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatMessage;

import java.util.List;

public interface ChatMessageDao {

    List<ChatMessage> findByChatRoom_Id(int chatId);

    void save(ChatMessage chatMessage);

    void deleteById(int id);

}
