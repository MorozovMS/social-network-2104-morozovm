package com.getjavajob.training.morozovm.socialnetwork.dao.connectionpool;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();

    boolean releaseConnection(Connection connection);

    String getUrl();

    String getUser();

    String getPassword();

    void clearUsedConnection();

    void closeAllConnections();

}
