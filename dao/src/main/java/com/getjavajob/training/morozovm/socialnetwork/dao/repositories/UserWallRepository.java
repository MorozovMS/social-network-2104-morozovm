package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserWallDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Primary
@RepositoryDefinition(domainClass = UserWallMessage.class, idClass = Integer.class)
public interface UserWallRepository extends UserWallDao {

    List<UserWallMessage> findByUserIdOrderByIdDesc(int id);

    @Query(value = "select uwm from UserWallMessage uwm where uwm.creator.id in " +
            "(select u from User u join u.friends uf where uf.id = :id) order by uwm.createdDate desc",
            countQuery = "select count(uwm) from UserWallMessage uwm where uwm.creator.id in " +
                    "(select u from User u join u.friends uf where uf.id = :id) order by uwm.createdDate desc")
    Page<UserWallMessage> findNews(@Param("id") int id, Pageable pageable);

}
