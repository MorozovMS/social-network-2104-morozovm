package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserWallDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class MySqlUserWallDao implements UserWallDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MySqlUserWallDao() {

    }

    @Override
    public UserWallMessage save(UserWallMessage message) {
        User user = entityManager.find(User.class, message.getUser().getId());
        user.getMessages().add(message);
        entityManager.merge(user);
        return message;
    }

    @Override
    public void delete(UserWallMessage message) {
        User user = entityManager.find(User.class, message.getUser().getId());
        message.setUser(user);
        entityManager.remove(entityManager.contains(message) ? message : entityManager.merge(message));
        entityManager.merge(user);
    }

    @Override
    public List<UserWallMessage> findByUserIdOrderByIdDesc(int id) {
        User user = entityManager.find(User.class, id);
        return user.getMessages();
    }

    @Override
    public Page<UserWallMessage> findNews(int id, Pageable pageable) {
        return null;
    }

}
