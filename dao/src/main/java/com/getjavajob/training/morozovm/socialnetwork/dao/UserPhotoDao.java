package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.UserPhoto;

public interface UserPhotoDao {

    boolean existsById(int id);

    UserPhoto save(UserPhoto userPhoto);

    UserPhoto getUserPhoto(int id);

    void deleteById(int id);

}
