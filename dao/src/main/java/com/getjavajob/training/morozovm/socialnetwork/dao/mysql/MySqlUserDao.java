package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.Phone;
import com.getjavajob.training.morozovm.socialnetwork.common.SearchResult;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Repository
public class MySqlUserDao implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MySqlUserDao() {

    }

    @Override
    public User save(User user) {
        entityManager.persist(user);
        return user;
    }

    @Override
    public User findById(int id) {
        try {
            return entityManager.createQuery("SELECT u from User u JOIN fetch u.phones where u.id = :id", User.class)
                    .setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            User user = entityManager.createQuery("SELECT u from User u where u.id = :id", User.class)
                    .setParameter("id", id).getSingleResult();
            user.setPhones(new ArrayList<>());
            return user;
        }
    }

    public int update(User user) {
        User oldUser = entityManager.find(User.class, user.getId());
        user.setGroups(oldUser.getGroups());
        List<Phone> oldPhones = oldUser.getPhones();
        List<Phone> newPhones = user.getPhones();
        for (Phone p : oldPhones) {
            if (newPhones.contains(p)) {
                entityManager.merge(p);
            } else {
                entityManager.remove(entityManager.contains(p) ? p : entityManager.merge(p));
            }
            newPhones.remove(p);
        }
        for (Phone p : newPhones) {
            entityManager.persist(p);
        }
        entityManager.merge(user);
        return user.getId();
    }

    @Override
    public void changePassword(int id, String password) {
        Query updPassQuery = entityManager.createQuery("UPDATE User u SET u.password = :pass WHERE u.id = :id");
        updPassQuery.setParameter("id", id);
        updPassQuery.setParameter("pass", password);
        updPassQuery.executeUpdate();
    }

    @Override
    public void deleteById(int id) {
        entityManager.merge(id);
        entityManager.remove(id);
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll(long offset, int limit) {
        Query queryForIds = entityManager.createQuery("Select u.id from User u order by u.surname");
        List<Integer> userIds = queryForIds.getResultList();
        Query query = entityManager.createQuery("Select u from User u where u.id in :ids");
        int end = (int) offset + limit;
        if ((offset + limit) > userIds.size()) {
            end = userIds.size();
        }
        query.setParameter("ids", userIds.subList((int) offset, end));
        return (List<User>) query.getResultList();
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return new PageImpl<>(findAll(pageable.getOffset(), pageable.getPageSize()));
    }

    @Override
    public User findByEmail(String email) {
        Query query = entityManager.createQuery("select u from User u where u.email = :email", User.class);
        query.setParameter("email", email);
        return (User) query.getSingleResult();
    }

    public long getCountUser() {
        Query query = entityManager.createQuery("select count(u.id) from User u");
        return (long) query.getSingleResult();
    }

    public long getCountUserGroups(int id) {
        return entityManager.find(User.class, id).getGroups().size();
    }

    public List<Group> getUserGroups(int id, int offset, int limit) {
        List<Group> groups = new ArrayList<>(entityManager.find(User.class, id).getGroups());
        groups.sort(Comparator.naturalOrder());
        int end = offset + limit;
        if ((offset + limit) > groups.size()) {
            end = groups.size();
        }
        return groups.subList(offset, end);
    }

    @Override
    public Page<User> findByGroupsId(int groupId, Pageable pageable) {
        List<User> members = new ArrayList<>(entityManager.find(Group.class, groupId).getMembers());
        int end = (int) pageable.getOffset() + pageable.getPageSize();
        if (((int) pageable.getOffset() + pageable.getPageSize()) > members.size()) {
            end = members.size();
        }
        members.sort(Collections.reverseOrder());
        return new PageImpl<>(members.subList((int) pageable.getOffset(), end));
    }

    @Override
    public boolean existsByEmail(String email) {
        return false;
    }

    @Override
    public boolean existsById(int id) {
        return false;
    }

    @Override
    public void deleteFriend(int id, int friendId) {

    }

    @Override
    public void addFriend(int id, int friendId) {

    }

    @Override
    public void sendFriendRequest(int id, int friendId) {

    }

    @Override
    public Page<User> findUserFriends(int id, Pageable pageable) {
        return null;
    }

    @Override
    public Set<User> findUserFriendRequest(int id) {
        return null;
    }

    @Override
    public Set<User> findUserInputFriendRequests(int id) {
        return null;
    }

    @Override
    public void deleteFriendRequest(int id, int friendId) {

    }

    @Override
    public int checkFriendship(int id, int friend_id) {
        return 0;
    }

    @Override
    public int checkFriendshipRequest(int id, int friend_id) {
        return 0;
    }

    @Override
    public void leaveGroup(int idUser, int idGroup) {
        User user = entityManager.find(User.class, idUser);
        Group group = entityManager.find(Group.class, idGroup);
        user.getGroups().remove(group);
        group.getMembers().remove(user);
        entityManager.merge(group);
        entityManager.merge(user);
    }

    @Override
    public void joinGroup(int idUser, int idGroup) {
        User user = entityManager.find(User.class, idUser);
        Group group = entityManager.find(Group.class, idGroup);
        user.getGroups().add(group);
        group.getMembers().add(user);
        entityManager.merge(group);
        entityManager.merge(user);
    }

    @Override
    public Page<SearchResult> search(String searchQuery, Pageable pageable) {
        return null;
    }

    @Override
    public int setReadOnlyMode(int id) {
        return 0;
    }

    @Override
    public int removeReadOnlyMode(int id) {
        return 0;
    }

    @Override
    public void banUser(int id) {

    }

    @Override
    public void unbanUser(int id) {

    }

    @Override
    public String getUserNameById(int id) {
        return null;
    }

}
