package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserFriendsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MySqlUserFriendsDao implements UserFriendsDao {

    private static final String GET_FRIENDS = "Select friend_id from user_friends where user_id = ?";
    private static final String BECOME_FRIENDS = "INSERT INTO user_friends (user_id, friend_id) VALUES(?, ?)";
    private static final String DELETE_FRIEND = "DELETE FROM user_friends Where user_id = ? AND friend_id = ?";
    private static final String UPDATE_FRIEND = "UPDATE user_friends SET friend_id = ? WHERE user_id = ? AND friend_id = ?";

    private JdbcTemplate jdbcTemplate;

    public MySqlUserFriendsDao() {

    }

    @Autowired
    public MySqlUserFriendsDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Integer> getFriends(User user) {
        return jdbcTemplate.query(GET_FRIENDS, new Object[]{user.getId()}, (resultSet, i) -> resultSet.getInt(1));
    }

    @Override
    public int becomeFriends(User user, User newFriend) {
        return jdbcTemplate.update(BECOME_FRIENDS, user.getId(), newFriend.getId());
    }

    @Override
    public int deleteFriend(User user, User lostFriend) {
        return jdbcTemplate.update(DELETE_FRIEND, user.getId(), lostFriend.getId());
    }

    @Override
    public int updateFriend(User user, User oldFriend, User newFriend) {
        return jdbcTemplate.update(UPDATE_FRIEND, newFriend.getId(), user.getId(), oldFriend.getId());
    }

}
