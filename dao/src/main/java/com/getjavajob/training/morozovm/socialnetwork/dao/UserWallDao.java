package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserWallDao {

    UserWallMessage save(UserWallMessage message);

    void delete(UserWallMessage message);

    List<UserWallMessage> findByUserIdOrderByIdDesc(int id);

    Page<UserWallMessage> findNews(int id, Pageable pageable);

}
