package com.getjavajob.training.morozovm.socialnetwork.dao.connectionpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;

public class MySqlConnectionPool implements ConnectionPool {

    private static final int POOL_SIZE = 8;
    private static final ArrayBlockingQueue<Connection> usedConnection = new ArrayBlockingQueue<>(POOL_SIZE);
    private static ArrayBlockingQueue<Connection> connectionPool;
    private final String url;
    private final String user;
    private final String password;

    private MySqlConnectionPool(String url, String user, String password, ArrayBlockingQueue<Connection> connectionPool) {
        this.url = url;
        this.user = user;
        this.password = password;
        MySqlConnectionPool.connectionPool = connectionPool;
    }

    public static MySqlConnectionPool create(String url, String user, String password) throws SQLException, ClassNotFoundException {
        ArrayBlockingQueue<Connection> pool = new ArrayBlockingQueue<>(POOL_SIZE);
        for (int i = 0; i < POOL_SIZE; i++) {
            pool.add(createConnection(url, user, password));
        }
        return new MySqlConnectionPool(url, user, password, pool);
    }

    private static Connection createConnection(String url, String user, String password) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection(url, user, password);
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return connection;
    }

    @Override
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = connectionPool.take();
            usedConnection.put(connection);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        try {
            connectionPool.put(connection);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return usedConnection.remove(connection);
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public int getPoolSize() {
        return connectionPool.size() + usedConnection.size();
    }

    @Override
    public void closeAllConnections() {
        for (Connection connection : connectionPool) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Override
    public void clearUsedConnection() {
        usedConnection.clear();
    }

}
