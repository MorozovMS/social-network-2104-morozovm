package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.SearchResult;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.UserDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

import java.util.Set;

@Primary
@RepositoryDefinition(domainClass = User.class, idClass = Integer.class)
public interface UserRepository extends UserDao {

    Page<User> findByGroupsId(int groupId, Pageable pageable);

    Page<User> findAll(Pageable pageable);

    boolean existsByEmail(String email);

    boolean existsById(int id);

    User findByEmail(String email);

    User findById(int id);

    @Modifying
    @Query(value = "delete from user_friends where (user_id = :id and friend_id = :friendId) " +
            "or (user_id = :friendId and friend_id = :id)", nativeQuery = true)
    void deleteFriend(@Param("id") int id, @Param("friendId") int friendId);

    @Modifying
    @Query(value = "insert into user_friends (user_id, friend_id) values (:id, :friendId), (:friendId, :id)",
            nativeQuery = true)
    void addFriend(@Param("id") int id, @Param("friendId") int friendId);

    @Modifying
    @Query(value = "insert into user_friends_requests (sender_id, responder_id) VALUES (:id, :friendId)",
            nativeQuery = true)
    void sendFriendRequest(@Param("id") int id, @Param("friendId") int friendId);

    @Query(value = "select u from User u join u.friends uf where uf.id = :id")
    Page<User> findUserFriends(@Param("id") int id, Pageable pageable);

    @Query(value = "select u.friendsRequests from User u where u.id = :id")
    Set<User> findUserFriendRequest(@Param("id") int id);

    @Query(value = "select u from User u join u.friendsRequests fr where fr.id =:id")
    Set<User> findUserInputFriendRequests(@Param("id") int id);

    @Modifying
    @Query(value = "delete from user_friends_requests where sender_id = :id and responder_id = :friendId",
            nativeQuery = true)
    void deleteFriendRequest(@Param("id") int id, @Param("friendId") int friendId);

    @Modifying
    @Query("UPDATE User u SET u.password = :password where u.id = :id")
    void changePassword(@Param("id") int id, @Param("password") String password);

    void deleteById(int id);

    @Query(value = "select exists(select * from user_friends where user_id = :id and friend_id = :friend_id)",
            nativeQuery = true)
    int checkFriendship(@Param("id") int id, @Param("friend_id") int friend_id);

    @Query(value = "select exists(select * from user_friends_requests where sender_id = :id and responder_id = :friend_id)",
            nativeQuery = true)
    int checkFriendshipRequest(@Param("id") int id, @Param("friend_id") int friend_id);

    @Modifying
    @Query(value = "delete from user_group where group_id = :idGroup and user_id = :id", nativeQuery = true)
    void leaveGroup(@Param("id") int id, @Param("idGroup") int idGroup);

    @Modifying
    @Query(value = "insert into user_group (user_id, group_id) values (:id, :idGroup)", nativeQuery = true)
    void joinGroup(@Param("id") int id, @Param("idGroup") int idGroup);

    @Query(value = "select name, surname, midname, id, 'user' as type from users where name like :searchQuery " +
            "or surname like :searchQuery or midname like :searchQuery " +
            "union" +
            " select name, 'group' as surname, 'group' as midname, id, 'group' as type from `groups` where name like :searchQuery",
            countQuery = "select count(id) from (select id from users where name like :searchQuery or " +
                    "surname like :searchQuery or midname like :searchQuery " +
                    "union" +
                    " select id from `groups` where name like :searchQuery) as res",
            nativeQuery = true)
    Page<SearchResult> search(@Param("searchQuery") String searchQuery, Pageable pageable);

    @Modifying
    @Query("update User u set u.role = 'ROLE_READONLY' where u.id = :id")
    int setReadOnlyMode(@Param("id") int id);

    @Modifying
    @Query("update User u set u.role = 'ROLE_USER' where u.id = :id")
    int removeReadOnlyMode(@Param("id") int id);

    @Modifying
    @Query("update User u set u.status = 'BANNED' where u.id = :id")
    void banUser(@Param("id") int id);

    @Modifying
    @Query("update User u set u.status = 'ACTIVE' where u.id = :id")
    void unbanUser(@Param("id") int id);

    @Query("select u.name from User u where u.id = :id")
    String getUserNameById(@Param("id") int id);

}
