package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GroupDao {

    Group save(Group group);

    Group findById(int id);

    void delete(Group group);

    Page<Group> findAll(Pageable pageable);

    int checkMembership(int idGroup, int idUser);

    Page<Group> getUserGroups(int id, Pageable pageable);

    int findCreatorId(int idGroup);

}
