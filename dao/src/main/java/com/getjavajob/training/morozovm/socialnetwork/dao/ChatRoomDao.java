package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ChatRoomDao {

    void save(ChatRoom chatRoom);

    int getChatId(int senderId, int recipientId);

    int checkExistsBySenderIdAndRecipientId(int senderId, int recipientId);

    Page<ChatRoom> getAllUsersChats(int id, Pageable pageable);

}
