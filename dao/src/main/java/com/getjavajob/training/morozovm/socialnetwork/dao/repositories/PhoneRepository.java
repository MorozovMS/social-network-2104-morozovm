package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Phone;
import com.getjavajob.training.morozovm.socialnetwork.dao.PhoneDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Primary
@RepositoryDefinition(domainClass = Phone.class, idClass = Integer.class)
public interface PhoneRepository extends PhoneDao {

    @Query("select p from Phone p where p.user.id = :id")
    List<Phone> getPhones(@Param("id") int id);

}
