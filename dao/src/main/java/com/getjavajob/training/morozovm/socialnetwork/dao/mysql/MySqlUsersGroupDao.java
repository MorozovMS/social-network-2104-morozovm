package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.UsersGroupDao;
import com.getjavajob.training.morozovm.socialnetwork.dao.mappers.UserGroupsExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MySqlUsersGroupDao implements UsersGroupDao {

    private static final String USERS_IN_GROUP = "Select user_id from user_group where group_id = ?";
    private static final String USER_GROUPS = "Select group_id from user_group where user_id = ?";
    private static final String JOIN_GROUP = "INSERT INTO user_group (user_id, group_id) VALUES(?, ?)";
    private static final String LEAVE_GROUP = "DELETE FROM user_group WHERE user_id = ? AND group_id = ?";
    private static final String UPDATE_GROUP = "UPDATE user_group SET group_id = ? WHERE user_id = ? AND group_id = ?";

    private JdbcTemplate jdbcTemplate;

    public MySqlUsersGroupDao() {
    }

    @Autowired
    public MySqlUsersGroupDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Integer> usersInGroup(Group group) {
        return jdbcTemplate.query(USERS_IN_GROUP, new Object[]{group.getId()}, new UserGroupsExtractor());
    }

    @Override
    public List<Integer> userGroups(User user) {
        return jdbcTemplate.query(USER_GROUPS, new Object[]{user.getId()}, new UserGroupsExtractor());
    }

    @Override
    public int joinGroup(Group group, User user) {
        return jdbcTemplate.update(JOIN_GROUP, user.getId(), group.getId());
    }

    @Override
    public int leaveGroup(Group group, User user) {
        return jdbcTemplate.update(LEAVE_GROUP, user.getId(), group.getId());
    }

    @Override
    public int updateGroup(User user, Group oldAcc, Group newAcc) {
        return jdbcTemplate.update(UPDATE_GROUP, newAcc.getId(), user.getId(), oldAcc.getId());
    }

}
