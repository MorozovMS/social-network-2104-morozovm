package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;

import java.util.List;

public interface UsersGroupDao {

    List<Integer> usersInGroup(Group group);

    List<Integer> userGroups(User user);

    int joinGroup(Group group, User user);

    int leaveGroup(Group group, User user);

    int updateGroup(User user, Group oldAcc, Group newAcc);

}
