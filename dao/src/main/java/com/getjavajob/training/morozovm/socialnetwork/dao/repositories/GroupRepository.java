package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

@Primary
@RepositoryDefinition(domainClass = Group.class, idClass = Integer.class)
public interface GroupRepository extends GroupDao {

    @Query("select g from Group g join g.members m where m.id = :id")
    Page<Group> getUserGroups(@Param("id") int id, Pageable pageable);

    @Query(value = "select exists(select * from user_group where user_id = :id and group_id = :idGroup)",
            nativeQuery = true)
    int checkMembership(@Param("idGroup") int idGroup, @Param("id") int id);

    @Query(value = "select g.creator.id from Group g where g.id = :idGroup")
    int findCreatorId(@Param("idGroup") int idGroup);

}
