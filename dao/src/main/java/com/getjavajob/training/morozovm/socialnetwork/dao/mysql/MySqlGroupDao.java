package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;

@Repository
public class MySqlGroupDao implements GroupDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MySqlGroupDao() {

    }

    @Override
    public Group save(Group group) {
        entityManager.persist(group);
        return group;
    }

    @Override
    public Group findById(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
        Root<Group> from = criteriaQuery.from(Group.class);
        criteriaQuery.select(from);
        criteriaQuery.where(criteriaBuilder.equal(from.get("id"), id));
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public int update(Group group) {
        entityManager.merge(group);
        return Objects.nonNull(group) ? 1 : 0;
    }

    @Override
    public void delete(Group group) {
        Object managed = entityManager.merge(group);
        entityManager.remove(managed);
    }

    @SuppressWarnings("unchecked")
    public List<Group> getAllGroups(long offset, int limit) {
        Query queryForIds = entityManager.createQuery("Select g.id from Group g order by g.name");
        List<Integer> groupIds = (List<Integer>) queryForIds.getResultList();
        Query query = entityManager.createQuery("Select g from Group g where g.id in :ids");
        int end = (int) offset + limit;
        if ((offset + limit) > groupIds.size()) {
            end = groupIds.size();
        }
        query.setParameter("ids", groupIds.subList((int) offset, end));
        return (List<Group>) query.getResultList();
    }

    @Override
    public Page<Group> findAll(Pageable pageable) {
        return new PageImpl<>(getAllGroups(pageable.getOffset(), pageable.getPageSize()));
    }

    public long getCountGroup() {
        Query query = entityManager.createQuery("select count(g.id) from Group g");
        return (long) query.getSingleResult();
    }

    public int getGroupMembersCount(int id) {
        return entityManager.find(Group.class, id).getMembers().size();
    }

    @Override
    public int checkMembership(int idGroup, int idUser) {
        if (entityManager.find(Group.class, idGroup).getMembers().contains(entityManager.find(User.class, idUser))) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Page<Group> getUserGroups(int id, Pageable pageable) {
        return null;
    }

    @Override
    public int findCreatorId(int idGroup) {
        return 0;
    }

}
