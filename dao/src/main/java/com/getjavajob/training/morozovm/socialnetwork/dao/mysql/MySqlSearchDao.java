package com.getjavajob.training.morozovm.socialnetwork.dao.mysql;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.dao.SearchDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MySqlSearchDao implements SearchDao {

    @PersistenceContext
    private EntityManager entityManager;

    public MySqlSearchDao() {

    }

    @Override
    public List<Object> search(String searchQuery, int offset, int limit) {
        String searchString = "%" + searchQuery + "%";
        TypedQuery<User> queryUsers = entityManager.createQuery("select u from User u " +
                "where u.name like :sq or u.surname like :sq or u.midName like :sq order by u.id", User.class);
        TypedQuery<Group> queryGroups = entityManager.createQuery("select g from Group g " +
                "where g.name like :sq order by g.id", Group.class);
        queryUsers.setParameter("sq", searchString);
        queryGroups.setParameter("sq", searchString);
        List<Object> result = new ArrayList<>();
        result.addAll(queryUsers.getResultList());
        result.addAll(queryGroups.getResultList());
        int end = offset + limit;
        if ((offset + limit) > result.size()) {
            end = result.size();
        }
        return result.subList(offset, end);
    }

    @Override
    public int getCount(String searchQuery) {
        String searchString = "%" + searchQuery + "%";
        Query queryUsers = entityManager.createQuery("select u.id from User u " +
                "where u.name like :sq or u.surname like :sq or u.midName like :sq");
        Query queryGroups = entityManager.createQuery("select g.id from Group g where g.name like :sq");
        queryUsers.setParameter("sq", searchString);
        queryGroups.setParameter("sq", searchString);
        return queryUsers.getResultList().size() + queryGroups.getResultList().size();
    }

}
