package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.GroupWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.dao.GroupWallDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.RepositoryDefinition;

import java.util.List;

@Primary
@RepositoryDefinition(domainClass = GroupWallMessage.class, idClass = Integer.class)
public interface GroupWallRepository extends GroupWallDao {

    List<GroupWallMessage> findByGroupIdOrderByIdDesc(int groupId);

}
