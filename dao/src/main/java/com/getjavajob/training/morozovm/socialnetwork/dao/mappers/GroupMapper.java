package com.getjavajob.training.morozovm.socialnetwork.dao.mappers;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupMapper implements RowMapper<Group> {

    @Override
    public Group mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(5));
        return new Group(resultSet.getInt(1), resultSet.getString(2),
                resultSet.getString(3), resultSet.getDate(4), user, null);
    }

}
