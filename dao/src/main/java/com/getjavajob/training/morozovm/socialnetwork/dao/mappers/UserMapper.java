package com.getjavajob.training.morozovm.socialnetwork.dao.mappers;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        return new User.Builder(resultSet.getInt(1), resultSet.getString(2),
                resultSet.getString(3), resultSet.getBoolean(11),
                resultSet.getString(5), resultSet.getString(8))
                .setMidName(resultSet.getString(4)).setSkype(resultSet.getString(6)).
                        setTelegram(resultSet.getString(7)).setBDay(resultSet.getDate(10)).
                        setRegDate(resultSet.getDate(9)).setHomeAddress(resultSet.getString(12)).
                        setWorkAddress(resultSet.getString(13)).setAddInfo(resultSet.getString(14)).
                        build();
    }

}
