package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.GroupPhoto;

public interface GroupPhotoDao {

    boolean existsById(int id);

    GroupPhoto save(GroupPhoto groupPhoto);

    GroupPhoto getGroupPhoto(int id);

    void deleteById(int id);

    void delete(GroupPhoto groupPhoto);

}
