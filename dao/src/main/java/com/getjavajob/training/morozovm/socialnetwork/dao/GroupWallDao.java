package com.getjavajob.training.morozovm.socialnetwork.dao;

import com.getjavajob.training.morozovm.socialnetwork.common.GroupWallMessage;

import java.util.List;

public interface GroupWallDao {

    GroupWallMessage save(GroupWallMessage message);

    void delete(GroupWallMessage message);

    List<GroupWallMessage> findByGroupIdOrderByIdDesc(int id);

}
