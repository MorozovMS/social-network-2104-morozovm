package com.getjavajob.training.morozovm.socialnetwork.dao.repositories;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatRoom;
import com.getjavajob.training.morozovm.socialnetwork.dao.ChatRoomDao;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

@Primary
@RepositoryDefinition(domainClass = ChatRoom.class, idClass = Long.class)
public interface ChatRoomRepository extends ChatRoomDao {

    @Query(value = "select cr.id from ChatRoom cr where (cr.sender.id = : senderId and cr.recipient.id = :recipientId) " +
            "or (cr.sender.id = :recipientId and cr.recipient.id = :senderId)")
    int getChatId(@Param("senderId") int senderId, @Param("recipientId") int recipientId);

    @Query(value = "select exists(select * from chat_room where (sender_id = :senderId and recipient_id = :recipientId) " +
            "or (sender_id = :recipientId and recipient_id = :senderId))", nativeQuery = true)
    int checkExistsBySenderIdAndRecipientId(@Param("senderId") int senderId, @Param("recipientId") int recipientId);

    @Query(value = "select distinct room from ChatRoom room where room.sender.id = :id or room.recipient.id = :id")
    Page<ChatRoom> getAllUsersChats(@Param("id") int id, Pageable pageable);

}
