package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.SearchResult;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
public class SearchController {

    private final SearchService searchService;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping(value = "/search")
    public ModelAndView search(@RequestParam Map<String, String> requestParams, HttpServletRequest request,
                               Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("searchResult");
        String searchQuery = requestParams.get("searchQuery");
        modelAndView.addObject("searchQuery", searchQuery);
        int currentPage;
        if (Objects.isNull(requestParams.get("currentPage"))) {
            currentPage = 1;
        } else {
            currentPage = Integer.parseInt(requestParams.get("currentPage"));
        }
        modelAndView.addObject("currentPage", currentPage);
        Page<SearchResult> searchResults = searchService.search(searchQuery, currentPage - 1);
        int numberOfPages;
        if (Objects.isNull(requestParams.get("numberOfPages"))) {
            numberOfPages = searchResults.getTotalPages();
        } else {
            numberOfPages = Integer.parseInt(requestParams.get("numberOfPages"));
        }
        modelAndView.addObject("numberOfPages", numberOfPages);
        List<String> searchView = new ArrayList<>();
        List<String> hrefs = new ArrayList<>();
        String stringToView = "";
        String href = "";
        for (SearchResult searchResult : searchResults.getContent()) {
            if (searchResult.getType().equals("user")) {
                stringToView = "User: " + searchResult.getSurname() + " " + searchResult.getName() + " " + searchResult.getMidname();
                href = request.getContextPath() + "/users/" + searchResult.getId();
            } else if (searchResult.getType().equals("group")) {
                stringToView = "Group: " + searchResult.getName();
                href = request.getContextPath() + "/groups/" + searchResult.getId();
            }
            searchView.add(stringToView);
            hrefs.add(href);
        }
        modelAndView.addObject("searchResultHrefs", hrefs);
        modelAndView.addObject("searchResultView", searchView);
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        return modelAndView;
    }

    @RequestMapping(value = "/search/page/{currentPage}")
    @ResponseBody
    public List<SearchResult> searchPagination(@RequestParam("searchQuery") String searchQuery,
                                               @PathVariable("currentPage") int currentPage) {
        return searchService.search(searchQuery, currentPage - 1).getContent();
    }

    @RequestMapping(value = "/search-autocomplete")
    @ResponseBody
    public List<SearchResult> searchAutocomplete(@RequestParam("filter") String filter) {
        return searchService.search(filter, 0).getContent();
    }

}
