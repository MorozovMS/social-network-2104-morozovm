package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.service.GroupService;
import com.getjavajob.training.morozovm.socialnetwork.service.PhotoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping(value = "/groups")
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    private final GroupService groupService;
    private final PhotoService photoService;

    @Autowired
    public GroupController(GroupService groupService, PhotoService photoService) {
        this.groupService = groupService;
        this.photoService = photoService;
    }

    @GetMapping(value = "/create")
    public ModelAndView createGroup(Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("createGroup");
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        return modelAndView;
    }

    @PostMapping(value = "/create")
    public String createGroupSubmit(@ModelAttribute Group group, @RequestParam("creatorId") int creatorId) {
        User user = new User();
        user.setId(creatorId);
        group.setCreator(user);
        int idGroup = groupService.createGroup(group);
        logger.info("Created group with id={}", idGroup);
        return "redirect:/groups/" + idGroup;
    }

    @GetMapping(value = "/{id}")
    public ModelAndView viewGroup(@PathVariable("id") int id, Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("group");
        boolean checkImage = photoService.checkGroupPhotoExists(id);
        modelAndView.addObject("checkImage", checkImage);
        Group group = groupService.readGroup(id);
        modelAndView.addObject("group", group);
        modelAndView.addObject("messages", groupService.getAllMessages(group));
        boolean membership = groupService.checkMembership(id, securityUser.getId());
        modelAndView.addObject("membership", membership);
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        modelAndView.addObject("authName", securityUser.getName());
        modelAndView.addObject("authSurname", securityUser.getSurname());
        return modelAndView;
    }

    @GetMapping(value = "/{id}/edit")
    public ModelAndView updateGroup(@PathVariable("id") int id, Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        Group group = groupService.readGroup(id);
        if (securityUser.getId() == group.getCreator().getId() || securityUser.isAdmin()) {
            ModelAndView modelAndView = new ModelAndView("updateGroup");
            modelAndView.addObject("authId", securityUser.getId());
            modelAndView.addObject("isAdmin", securityUser.isAdmin());
            boolean checkImage = photoService.checkGroupPhotoExists(id);
            modelAndView.addObject("checkImage", checkImage);
            modelAndView.addObject("group", group);
            return modelAndView;
        } else {
            return new ModelAndView("/accessDenied");
        }
    }

    @PostMapping(value = "/{id}")
    public String saveChangesGroup(@ModelAttribute Group group, @RequestParam("creatorId") int creatorId) {
        User user = new User();
        user.setId(creatorId);
        group.setCreator(user);
        if (groupService.updateGroup(group)) {
            logger.info("Group with id={} updated", group.getId());
            return "redirect:/groups/" + group.getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/{id}/delete")
    public String deleteGroup(@ModelAttribute Group group, @RequestParam("creatorId") int creatorId) {
        User user = new User();
        user.setId(creatorId);
        group.setCreator(user);
        if (groupService.deleteGroup(group)) {
            logger.info("Group with id={} deleted", group.getId());
            return "redirect:/users/" + group.getCreator().getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    @GetMapping(value = "")
    public ModelAndView allGroups(Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("allGroups");
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        Page<Group> groups = groupService.getAllGroups(0);
        modelAndView.addObject("numberOfPages", groups.getTotalPages());
        modelAndView.addObject("currentPage", 1);
        return modelAndView.addObject("groups", groups.getContent());
    }

    @RequestMapping(value = "/page/{currentPage}")
    @ResponseBody
    public List<Group> allGroupPage(@PathVariable("currentPage") int currentPage) {
        return groupService.getAllGroups(currentPage - 1).getContent();
    }

    @GetMapping(value = "/{id}/image")
    public void getPhoto(@PathVariable("id") int id, HttpServletResponse response) {
        byte[] content = photoService.readGroupPhoto(id);
        try {
            response.getOutputStream().write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping(value = "/{id}/image/delete")
    public String deletePhoto(@PathVariable("id") int id) {
        if (photoService.deleteGroupPhoto(id)) {
            logger.info("Group (id={}) image was deleted", id);
            return "redirect:/groups/" + id;
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/{id}/image")
    public String uploadPhoto(@PathVariable int id, @RequestParam("file") MultipartFile file) throws IOException {
        if (photoService.checkSizePhoto(file.getSize())) {
            if (photoService.downloadGroupPhoto(id, file.getInputStream()) != -1) {
                logger.info("Image for group (id={}) was uploaded", id);
            } else {
                return "redirect:/access-denied";
            }
        } else {
            logger.debug("Image for group (id={}) don't downloaded. File size = {}, file name = {}",
                    id, file.getSize(), file.getName());
        }
        return "redirect:/groups/" + id;
    }

    @PostMapping(value = "/{id}/image/update")
    public String updatePhoto(@PathVariable int id, @RequestParam("file") MultipartFile file) throws IOException {
        if (photoService.checkSizePhoto(file.getSize())) {
            if (photoService.updateGroupPhoto(id, file.getInputStream()) != -1) {
                logger.info("Image for group (id={}) was updated", id);
            } else {
                return "redirect:/access-denied";
            }
        } else {
            logger.debug("Image for group (id={}) don't updated. File size = {}, file name = {}",
                    id, file.getSize(), file.getName());
        }
        return "redirect:/groups/" + id;
    }

    @GetMapping(value = "/{id}/members")
    public ModelAndView groupMembers(@PathVariable("id") int groupId, Authentication authentication,
                                     @RequestParam Map<String, String> requestParams) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("/groupMembers");
        boolean membership;
        if (Objects.isNull(requestParams.get("membership"))) {
            membership = groupService.checkMembership(groupId, securityUser.getId());
            modelAndView.addObject("membership", membership);
        } else {
            membership = requestParams.get("membership").equals("true");
        }
        modelAndView.addObject("membership", membership);
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        Page<User> users = groupService.getGroupMembers(groupId, 0);
        modelAndView.addObject("numberOfPages", users.getTotalPages());
        modelAndView.addObject("currentPage", 1);
        modelAndView.addObject("groupId", groupId);
        return modelAndView.addObject("members", users.getContent());
    }

    @RequestMapping(value = "/{id}/members/page/{currentPage}")
    @ResponseBody
    public List<User> groupMembersPage(@PathVariable("currentPage") int currentPage,
                                       @PathVariable("id") int id) {
        return groupService.getGroupMembers(id, currentPage - 1).getContent();
    }

    @PostMapping(value = "/{id}/leave")
    public String leaveGroup(@PathVariable("id") int groupId, @RequestParam("userId") int userId) {
        if (groupService.leaveGroup(groupId, userId)) {
            logger.info("User (id={}) leave group (id={})", userId, groupId);
            return "redirect:/groups/" + groupId;
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/{id}/join")
    public String joinGroup(@PathVariable("id") int groupId, @RequestParam("userId") int userId) {
        if (groupService.joinGroup(groupId, userId)) {
            logger.info("User (id={}) join group (id={})", userId, groupId);
            return "redirect:/groups/" + groupId;
        } else {
            return "redirect:/access-denied";
        }
    }

}
