package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasAuthority('users:admin')")
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    private final UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "")
    public ModelAndView helloAdmin(Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("admin");
        modelAndView.addObject("authId", securityUser.getId());
        return modelAndView;
    }

    @PostMapping(value = "/users/read-only")
    public ModelAndView setReadOnlyMode(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("admin");
        if (userService.setReadOnlyMode(id) == 1) {
            logger.info("Set read-only mode for user id={}", id);
            modelAndView.addObject("success", true);
        } else {
            modelAndView.addObject("error", true);
        }
        return modelAndView;
    }

    @PostMapping(value = "/users/read-only/remove")
    public ModelAndView removeReadOnlyMode(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("admin");
        if (userService.removeReadOnlyMode(id) == 1) {
            logger.info("Remove read-only mode for user id={}", id);
            modelAndView.addObject("success", true);
        } else {
            modelAndView.addObject("error", true);
        }
        return modelAndView;
    }

    @PostMapping(value = "/users/remove")
    public ModelAndView removeUser(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("admin");
        if (userService.isExistsById(id)) {
            userService.deleteUser(id);
            logger.info("Remove user id={}", id);
            modelAndView.addObject("success", true);
        } else {
            modelAndView.addObject("error", true);
        }
        return modelAndView;
    }

    @PostMapping(value = "/users/ban")
    public ModelAndView banUser(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("admin");
        if (!userService.isExistsById(id)) {
            modelAndView.addObject("error", true);
        } else {
            userService.banUser(id);
            logger.info("User id={} banned", id);
            modelAndView.addObject("success", true);
        }
        return modelAndView;
    }

    @PostMapping(value = "/users/ban/remove")
    public ModelAndView unbanUser(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("admin");
        if (!userService.isExistsById(id)) {
            modelAndView.addObject("error", true);
        } else {
            userService.unbanUser(id);
            logger.info("User id={} unbanned", id);
            modelAndView.addObject("success", true);
        }
        return modelAndView;
    }

}
