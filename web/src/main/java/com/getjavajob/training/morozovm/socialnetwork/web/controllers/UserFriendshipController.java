package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Controller
@RequestMapping(value = "/users/{id}/friends")
public class UserFriendshipController {

    private static final Logger logger = LoggerFactory.getLogger(UserFriendshipController.class);

    private final UserService userService;

    @Autowired
    public UserFriendshipController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "")
    public ModelAndView allFriends(@PathVariable("id") int id, Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("/userFriends");
        Page<User> friends = userService.getAllFriends(id, 0);
        modelAndView.addObject("friends", friends.getContent());
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        modelAndView.addObject("numberOfPages", friends.getTotalPages());
        modelAndView.addObject("currentPage", 1);
        modelAndView.addObject("id", id);
        return modelAndView;
    }

    @RequestMapping(value = "/page/{currentPage}")
    @ResponseBody
    public List<User> allFriendsPage(@PathVariable("id") int id, @PathVariable("currentPage") int currentPage) {
        return userService.getAllFriends(id, currentPage - 1).getContent();
    }

    @PostMapping(value = "/delete")
    public String deleteFriend(Authentication authentication, @PathVariable("id") int friendId) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        int id = securityUser.getId();
        if (userService.deleteFriend(id, friendId)) {
            logger.info("User (id={}) delete friend (id={})", id, friendId);
            return "redirect:/users/" + friendId;
        } else {
            return "redirect:/access-denied";
        }
    }

    @GetMapping(value = "/requests")
    public ModelAndView friendRequests(@PathVariable("id") int id, Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        Set<User> friendRequests = userService.getAllFriendRequests(id);
        Set<User> inputFriendRequests = userService.getAllInputFriendRequests(id);
        if (Objects.isNull(friendRequests) || Objects.isNull(inputFriendRequests)) {
            return new ModelAndView("/accessDenied");
        } else {
            ModelAndView modelAndView = new ModelAndView("/friendRequests");
            modelAndView.addObject("authId", id);
            modelAndView.addObject("isAdmin", securityUser.isAdmin());
            modelAndView.addObject("outgoingRequests", friendRequests);
            modelAndView.addObject("inputRequests", inputFriendRequests);
            return modelAndView;
        }
    }

    @PostMapping(value = "/request")
    public String sendFriendRequest(Authentication authentication, @PathVariable("id") int friendId) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        int id = securityUser.getId();
        if (userService.sendFriendRequest(id, friendId)) {
            return "redirect:/users/" + friendId;
        } else {
            return "redirect:/access-denied";
        }
    }

    @RequestMapping(value = "/requests/delete")
    public String deleteFriendRequest(@PathVariable("id") int friendId, @RequestParam("id") int id) {
        if (userService.deleteFriendRequest(id, friendId)) {
            return "redirect:/users/{id}/friends/requests/";
        } else {
            return "redirect:/access-denied";
        }
    }

    @RequestMapping(value = "/requests/decline")
    public String declineFriendRequest(@RequestParam("friendId") int friendId, @PathVariable("id") int id) {
        if (userService.deleteFriendRequest(friendId, id)) {
            return "redirect:/users/{id}/friends/requests";
        } else {
            return "redirect:/access-denied";
        }
    }

    @RequestMapping(value = "/requests/accept")
    public String acceptFriendRequest(@RequestParam("friendId") int friendId, @PathVariable("id") int id) {
        if (userService.acceptFriendRequest(id, friendId)) {
            userService.deleteFriendRequest(id, friendId);
            userService.deleteFriendRequest(friendId, id);
            logger.info("User (id={}) add friend (id={})", id, friendId);
            return "redirect:/users/{id}/friends/requests";
        } else {
            return "redirect:/access-denied";
        }
    }

}
