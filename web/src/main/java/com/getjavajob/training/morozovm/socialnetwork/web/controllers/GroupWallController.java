package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.Group;
import com.getjavajob.training.morozovm.socialnetwork.common.GroupWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping(value = "/groups/{id}/messages")
public class GroupWallController {

    private static final Logger logger = LoggerFactory.getLogger(GroupWallController.class);

    private final GroupService groupService;

    @Autowired
    public GroupWallController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping(value = "/message")
    public String sendMessage(@ModelAttribute GroupWallMessage message, @RequestParam Map<String, String> requestParams) {
        configureMessage(message, requestParams);
        groupService.sendMessage(message);
        logger.info("Wall message was sent to group (id={}) wall", message.getGroup().getId());
        return "redirect:/groups/" + message.getGroup().getId();
    }

    @PostMapping(value = "/message/delete")
    public String deleteMessage(@ModelAttribute GroupWallMessage message, @RequestParam Map<String, String> requestParams) {
        configureMessage(message, requestParams);
        if (groupService.deleteMessage(message)) {
            logger.info("Wall message was deleted in group (id={}) wall", message.getGroup().getId());
            return "redirect:/groups/" + message.getGroup().getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/message/update")
    public String updateMessage(@ModelAttribute GroupWallMessage message, @RequestParam Map<String, String> requestParams) {
        configureMessage(message, requestParams);
        if (groupService.updateMessage(message)) {
            logger.info("Wall message was updated in group (id={}) wall", message.getGroup().getId());
            return "redirect:/groups/" + message.getGroup().getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    private void configureMessage(GroupWallMessage message, Map<String, String> requestParams) {
        Group group = new Group();
        group.setId(Integer.parseInt(requestParams.get("groupId")));
        message.setGroup(group);
        User user = new User();
        user.setId(Integer.parseInt(requestParams.get("creatorId")));
        message.setCreator(user);
    }

}
