package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.Phone;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    private final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/")
    public ModelAndView helloPage(@RequestParam Map<String, String> requestParams, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("index");
        if (requestParams.containsKey("error")) {
            modelAndView.addObject("checkLogPas", false);
        }
        if (Objects.nonNull(authentication) && authentication.isAuthenticated()) {
            SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
            return new ModelAndView("redirect:/users/" + securityUser.getId());
        }
        return modelAndView;
    }

    @GetMapping(value = "/login-success")
    public String submitEnter(Authentication authentication) {
        return "redirect:/users/" + ((SecurityUser) authentication.getPrincipal()).getId();
    }

    @GetMapping(value = "/register")
    public ModelAndView registerPage(@RequestParam Map<String, String> requestParams) {
        ModelAndView modelAndView = new ModelAndView("/register");
        if (Objects.nonNull(requestParams.get("checkEmail")) && requestParams.get("checkEmail").equals("false")) {
            modelAndView.addObject("checkEmail", false);
        }
        return modelAndView;
    }

    @PostMapping(value = "/register")
    public String registerUser(@ModelAttribute User user, @RequestParam Map<String, String> requestParams,
                               HttpSession session, RedirectAttributes redirectAttributes) {
        if (!userService.isEmailExists(user.getEmail())) {
            user.setSex(requestParams.get("gender").equals("male"));
            Set<Phone> phones = new HashSet<>();
            int numberOfPhones = Integer.parseInt(requestParams.get("phonesCounter"));
            int i = 1;
            while (i <= numberOfPhones) {
                String phone = requestParams.get("phone" + i);
                if (Objects.nonNull(phone) && !phone.isEmpty()) {
                    phones.add(new Phone(0, phone, user));
                }
                i++;
            }
            user.setPhones(new ArrayList<>(phones));
            int id = userService.createUser(user);
            session.setAttribute("id", Integer.toString(id));
            session.setAttribute("name", user.getName());
            session.setAttribute("surname", user.getSurname());
            logger.info("Registered user with id {}", id);
            return "redirect:/users/" + id;
        } else {
            redirectAttributes.addAttribute("checkEmail", false);
            logger.info("Email (email={}) exists", user.getEmail());
            return "redirect:/register";
        }
    }

    @GetMapping(value = "/logout")
    public String doLogout(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        session.invalidate();
        for (Cookie c : request.getCookies()) {
            c.setMaxAge(0);
            response.addCookie(c);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/access-denied")
    public ModelAndView accessDenied() {
        return new ModelAndView("accessDenied");
    }

}
