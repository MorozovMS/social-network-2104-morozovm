package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.ChatMessage;
import com.getjavajob.training.morozovm.socialnetwork.common.ChatRoom;
import com.getjavajob.training.morozovm.socialnetwork.common.SecurityUser;
import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.service.ChatMessageService;
import com.getjavajob.training.morozovm.socialnetwork.service.ChatRoomService;
import com.getjavajob.training.morozovm.socialnetwork.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
public class ChatController {

    private final SimpMessagingTemplate messagingTemplate;
    private final ChatMessageService chatMessageService;
    private final ChatRoomService chatRoomService;
    private final UserService userService;

    @Autowired
    public ChatController(SimpMessagingTemplate messagingTemplate, ChatMessageService chatMessageService,
                          ChatRoomService chatRoomService, UserService userService) {
        this.messagingTemplate = messagingTemplate;
        this.chatMessageService = chatMessageService;
        this.chatRoomService = chatRoomService;
        this.userService = userService;
    }

    @MessageMapping("/chat")
    public void processMessage(@Payload ChatMessage chatMessage, @Payload Map<String, String> payload) {
        ChatRoom chatRoom = new ChatRoom();
        chatRoom.setId(Integer.parseInt(payload.get("chatId")));
        User sender = new User();
        User recipient = new User();
        sender.setId(Integer.parseInt(payload.get("senderId")));
        recipient.setId(Integer.parseInt(payload.get("recipientId")));
        chatMessage.setChatRoom(chatRoom);
        chatMessage.setSender(sender);
        chatMessage.setRecipient(recipient);
        chatMessageService.save(chatMessage);
        messagingTemplate.convertAndSendToUser(payload.get("recipientId"), "/messages", chatMessage);
    }

    @GetMapping(value = "users/{id}/chat")
    public ModelAndView chat(@RequestParam("to") int recipientId, Authentication authentication, @PathVariable int id) {
        int chatId = chatRoomService.getChatId(id, recipientId);
        if (chatId == -1) {
            return new ModelAndView("accessDenied");
        } else {
            SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
            ModelAndView modelAndView = new ModelAndView("chat");
            modelAndView.addObject("chatId", chatId);
            modelAndView.addObject("authId", securityUser.getId());
            modelAndView.addObject("isAdmin", securityUser.isAdmin());
            modelAndView.addObject("recipientId", recipientId);
            modelAndView.addObject("messages", chatMessageService.findChatMessages(chatId));
            modelAndView.addObject("senderName", userService.getNameById(securityUser.getId()));
            modelAndView.addObject("recipientName", userService.getNameById(recipientId));
            return modelAndView;
        }
    }

    @GetMapping(value = "users/{id}/chats")
    public ModelAndView allChats(@PathVariable int id, Authentication authentication) {
        Page<ChatRoom> chatRooms = chatRoomService.getChatRooms(id, 0);
        if (Objects.nonNull(chatRooms)) {
            SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
            ModelAndView modelAndView = new ModelAndView("allChats");
            modelAndView.addObject("authId", securityUser.getId());
            modelAndView.addObject("isAdmin", securityUser.isAdmin());
            modelAndView.addObject("numberOfPages", chatRooms.getTotalPages());
            modelAndView.addObject("currentPage", 1);
            modelAndView.addObject("chats", chatRooms.getContent());
            return modelAndView;
        } else {
            return new ModelAndView("accessDenied");
        }
    }

    @RequestMapping(value = "/users/{id}/chats/page/{currentPage}")
    @ResponseBody
    public List<ChatRoom> allChatsPage(@PathVariable int id, @PathVariable("currentPage") int currentPage) {
        return chatRoomService.getChatRooms(id, currentPage - 1).getContent();
    }

}