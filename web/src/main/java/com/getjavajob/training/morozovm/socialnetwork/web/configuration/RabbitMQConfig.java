package com.getjavajob.training.morozovm.socialnetwork.web.configuration;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    static final String qName = "soc-net-queue";
    static final String dlqName = "dlq-soc-net";
    static final String dlqExchange = "dlq-exchange";

    @Bean
    Queue queue() {
        return QueueBuilder.durable(qName)
                .withArgument("x-dead-letter-exchange", dlqExchange)
                .withArgument("x-dead-letter-routing-key", dlqName)
                .withArgument("x-message-ttl", 1000)
                .build();
    }

}
