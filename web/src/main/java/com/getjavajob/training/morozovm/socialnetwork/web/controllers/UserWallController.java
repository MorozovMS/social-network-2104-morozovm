package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.User;
import com.getjavajob.training.morozovm.socialnetwork.common.UserWallMessage;
import com.getjavajob.training.morozovm.socialnetwork.service.CacheService;
import com.getjavajob.training.morozovm.socialnetwork.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping(value = "/users/{id}/messages")
public class UserWallController {

    private static final Logger logger = LoggerFactory.getLogger(UserWallController.class);

    private final UserService userService;
    private final CacheService cacheService;

    @Autowired
    public UserWallController(UserService userService, CacheService cacheService) {
        this.userService = userService;
        this.cacheService = cacheService;
    }

    @PostMapping(value = "/message")
    public String sendMessageOnUserWall(@ModelAttribute UserWallMessage message,
                                        @RequestParam Map<String, String> requestParams) {
        configureMessage(message, requestParams);
        userService.sendMessage(message);
        cacheService.addToCache(Integer.parseInt(requestParams.get("creator_id")), message);
        logger.info("User wall message was sent to user (id={}) wall", message.getId());
        return "redirect:/users/" + message.getUser().getId();
    }

    @PostMapping(value = "/message/delete")
    public String deleteMessageFromUserWall(@ModelAttribute UserWallMessage message,
                                            @RequestParam Map<String, String> requestParams) {
        configureMessage(message, requestParams);
        if (userService.deleteMessage(message)) {
            cacheService.removeFromCache(Integer.parseInt(requestParams.get("creator_id")), message);
            logger.info("User wall message was deleted on user (id={}) wall", message.getId());
            return "redirect:/users/" + message.getUser().getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/message/update")
    public String updateMessageOnUserWall(@ModelAttribute UserWallMessage message,
                                          @RequestParam Map<String, String> requestParams) {
        configureMessage(message, requestParams);
        if (Objects.nonNull(userService.updateMessage(message))) {
            logger.info("User wall message was updated on user (id={}) wall", message.getId());
            return "redirect:/users/" + message.getUser().getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    private void configureMessage(@ModelAttribute UserWallMessage message, @RequestParam Map<String, String> requestParams) {
        User creator = new User();
        creator.setId(Integer.parseInt(requestParams.get("creator_id")));
        User user = new User();
        user.setId(Integer.parseInt(requestParams.get("user_id")));
        message.setCreator(creator);
        message.setUser(user);
    }

}
