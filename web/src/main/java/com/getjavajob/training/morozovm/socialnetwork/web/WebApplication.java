package com.getjavajob.training.morozovm.socialnetwork.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@ComponentScan(basePackages = {"com.getjavajob.training.morozovm.socialnetwork.common",
        "com.getjavajob.training.morozovm.socialnetwork.dao",
        "com.getjavajob.training.morozovm.socialnetwork.service",
        "com.getjavajob.training.morozovm.socialnetwork.web"})
@SpringBootApplication
@PropertySources(value = {@PropertySource("classpath:application.yml")})
@EnableCaching
public class WebApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class);
    }

}
