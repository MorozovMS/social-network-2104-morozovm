package com.getjavajob.training.morozovm.socialnetwork.web.controllers;

import com.getjavajob.training.morozovm.socialnetwork.common.*;
import com.getjavajob.training.morozovm.socialnetwork.service.CacheService;
import com.getjavajob.training.morozovm.socialnetwork.service.PhotoService;
import com.getjavajob.training.morozovm.socialnetwork.service.UserService;
import com.getjavajob.training.morozovm.socialnetwork.service.UsersXMLMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping(value = "/users")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final PhotoService photoService;
    private final UserService userService;
    private final UsersXMLMapper usersXMLMapper;
    private final CacheService cacheService;

    @Autowired
    public UserController(PhotoService photoService, UserService userService, UsersXMLMapper usersXMLMapper, CacheService cacheService) {
        this.photoService = photoService;
        this.userService = userService;
        this.usersXMLMapper = usersXMLMapper;
        this.cacheService = cacheService;
    }

    @GetMapping(value = "")
    public ModelAndView allUsers(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("/allUsers");
        Page<User> users = userService.getAllUsers(0);
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("numberOfPages", users.getTotalPages());
        modelAndView.addObject("currentPage", 1);
        modelAndView.addObject("users", users.getContent());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        return modelAndView;
    }

    @RequestMapping(value = "/page/{currentPage}")
    @ResponseBody
    public List<User> allUsersPage(@PathVariable("currentPage") int currentPage) {
        return userService.getAllUsers(currentPage - 1).getContent();
    }

    @GetMapping(value = "/{id}")
    public ModelAndView showUser(@PathVariable("id") int id, Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        int authId = securityUser.getId();
        ModelAndView modelAndView = new ModelAndView("/user");
        modelAndView.addObject("authId", authId);
        modelAndView.addObject("authName", securityUser.getName());
        modelAndView.addObject("authSurname", securityUser.getSurname());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        return getUserModelView(id, authId, modelAndView);
    }

    @GetMapping(value = "/{id}/news")
    public ModelAndView showNews(@PathVariable("id") int id, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("/news");
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        int authId = securityUser.getId();
        modelAndView.addObject("authId", authId);
        modelAndView.addObject("authName", securityUser.getName());
        modelAndView.addObject("authSurname", securityUser.getSurname());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        modelAndView.addObject("news", cacheService.getElements(id));
        modelAndView.addObject("numberOfPages", cacheService.getTotalPages(id));
        modelAndView.addObject("currentPage", 1);
        return modelAndView;
    }

    @RequestMapping(value = "/{id}/news/page/{currentPage}")
    @ResponseBody
    public Set<UserWallMessageDTO> newsPage(@PathVariable("currentPage") int currentPage,
                                            @PathVariable("id") int id) {
        return cacheService.getCachedElements(id, currentPage);
    }

    @GetMapping(value = "/{id}/user-groups")
    public ModelAndView userGroups(@PathVariable("id") int id, Authentication authentication) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        ModelAndView modelAndView = new ModelAndView("/userGroups");
        modelAndView.addObject("authId", securityUser.getId());
        modelAndView.addObject("isAdmin", securityUser.isAdmin());
        modelAndView.addObject("id", id);
        Page<Group> groups = userService.getUserGroups(id, 0);
        modelAndView.addObject("groups", groups.getContent());
        modelAndView.addObject("numberOfPages", groups.getTotalPages());
        modelAndView.addObject("currentPage", 1);
        return modelAndView;
    }

    @RequestMapping(value = "/{id}/user-groups/page/{currentPage}")
    @ResponseBody
    public List<Group> allUsersPage(@PathVariable("currentPage") int currentPage,
                                    @PathVariable("id") int id) {
        return userService.getUserGroups(id, currentPage - 1).getContent();
    }

    @GetMapping(value = "/{id}/edit")
    public ModelAndView showUpdateUser(Authentication authentication, @PathVariable("id") int id) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        int authId = securityUser.getId();
        if (id == authId) {
            ModelAndView modelAndView = new ModelAndView("/updateUser");
            modelAndView.addObject("authId", authId);
            modelAndView.addObject("isAdmin", securityUser.isAdmin());
            return getUserModelView(authId, authId, modelAndView);
        } else {
            return new ModelAndView("accessDenied");
        }
    }

    @PostMapping(value = "/{id}")
    public String saveChanges(@ModelAttribute User user, @RequestParam Map<String, String> requestParams) {
        getUserModelUpdate(user, requestParams);
        if (userService.updateUser(user) > 0) {
            logger.info("User (id={}) was updated", user.getId());
            return "redirect:/users/" + user.getId();
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/{id}/password")
    public String submitChangePassword(@RequestParam("password") String password, @PathVariable("id") int id) {
        if (userService.updatePassword(id, password)) {
            logger.info("User (id={}) password was updated", id);
            return "redirect:/users/" + id;
        } else {
            return "redirect:/access-denied";
        }
    }

    @GetMapping(value = "/{id}/password")
    public ModelAndView changePassword(Authentication authentication, @PathVariable("id") int id) {
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        if (id == securityUser.getId()) {
            ModelAndView modelAndView = new ModelAndView("/updateUserPassword");
            modelAndView.addObject("authId", securityUser.getId());
            modelAndView.addObject("isAdmin", securityUser.isAdmin());
            return modelAndView;
        } else {
            return new ModelAndView("accessDenied");
        }
    }

    @PostMapping(value = "/{id}/delete")
    public String deleteUser(@PathVariable("id") int id) {
        if (userService.deleteUser(id)) {
            logger.info("User (id={}) was deleted", id);
            return "redirect:/logout";
        } else {
            logger.info("User (id={}) was not deleted", id);
            return "redirect:/users/" + id;
        }
    }

    @GetMapping(value = "/{id}/image")
    public void getPhoto(@PathVariable("id") int id, HttpServletResponse response) {
        byte[] content = photoService.readUserPhoto(id);
        try {
            response.getOutputStream().write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping(value = "/{id}/image/delete")
    public String deletePhoto(@PathVariable("id") int id) {
        if (photoService.deleteUserPhoto(id)) {
            logger.info("User (id={}) image was deleted", id);
            return "redirect:/users/" + id;
        } else {
            return "redirect:/access-denied";
        }
    }

    @PostMapping(value = "/{id}/image")
    public String uploadPhoto(@RequestParam("file") MultipartFile file, @PathVariable("id") int id) throws IOException {
        if (photoService.checkSizePhoto(file.getSize())) {
            if (photoService.downloadUserPhoto(id, file.getInputStream()) > 0) {
                logger.info("User (id={}) image was downloaded", id);
            } else {
                return "redirect:/access-denied";
            }
        } else {
            logger.debug("Image for user (id={}) don't downloaded. File size = {}, file name = {}",
                    id, file.getSize(), file.getName());
        }
        return "redirect:/users/" + id;
    }

    @PostMapping(value = "/{id}/image/update")
    public String updatePhoto(@RequestParam("file") MultipartFile file, @PathVariable("id") int id) throws IOException {
        if (photoService.checkSizePhoto(file.getSize())) {
            if (photoService.updateUserPhoto(id, file.getInputStream()) > 0) {
                logger.info("User (id={}) image was updated", id);
            } else {
                return "redirect:/access-denied";
            }
        } else {
            logger.debug("Image for user (id={}) don't updated. File size = {}, file name = {}",
                    id, file.getSize(), file.getName());
        }
        return "redirect:/users/" + id;
    }

    @GetMapping(value = "/{id}/xml/export", produces = MediaType.APPLICATION_XML_VALUE)
    public void exportToXml(HttpServletResponse response, @PathVariable("id") int id) {
        try {
            response.setHeader("Content-Disposition", "attachment; filename=user_id=" + id + ".xml");
            usersXMLMapper.userToXML(userService.readUser(id), response.getOutputStream());
            response.flushBuffer();
            logger.info("User (id={}) was exported to XML", id);
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("Export user (id={}) to XML was failed", id);
        }
    }

    @PostMapping(value = "/{id}/xml/import")
    public ModelAndView importFromXml(@RequestParam("file") MultipartFile file,
                                      @PathVariable("id") int id) throws IOException {
        User user = usersXMLMapper.XMLToUser(file.getInputStream());
        ModelAndView modelAndView = new ModelAndView("/updateUser");
        boolean checkImage = photoService.checkUserPhotoExists(id);
        modelAndView.addObject("checkImage", checkImage);
        modelAndView.addObject("user", user);
        modelAndView.addObject("userPhones", user.getPhones());
        logger.info("User information was imported from XML");
        return modelAndView;
    }

    private ModelAndView getUserModelView(int id, int authId, ModelAndView modelAndView) {
        User user = userService.readUser(id);
        boolean checkImage = photoService.checkUserPhotoExists(id);
        if (id != authId) {
            if (userService.checkFriendship(id, authId)) {
                modelAndView.addObject("checkFriendship", true);
            } else {
                modelAndView.addObject("checkFriendship", false);
                modelAndView.addObject("checkFriendshipRequest", userService.checkFriendshipRequest(authId, id));
            }
            modelAndView.addObject("chatTo", true);
        } else {
            modelAndView.addObject("chatTo", false);
        }
        modelAndView.addObject("authId", authId);
        modelAndView.addObject("messages", user.getMessages());
        modelAndView.addObject("checkImage", checkImage);
        modelAndView.addObject("user", user);
        modelAndView.addObject("userPhones", user.getPhones());
        return modelAndView;
    }

    private void getUserModelUpdate(User user, Map<String, String> requestParams) {
        user.setSex(requestParams.get("gender").equals("male"));
        Set<Phone> phones = new HashSet<>();
        int numberOfPhones = Integer.parseInt(requestParams.get("phonesCounter"));
        int i = 1;
        while (i <= numberOfPhones) {
            String phone = requestParams.get("phone" + i);
            if (Objects.nonNull(phone) && !phone.isEmpty()) {
                phones.add(new Phone(Integer.parseInt(requestParams.get("phone_id" + i)), phone, user));
            }
            i++;
        }
        user.setPhones(new ArrayList<>(phones));
    }

}
