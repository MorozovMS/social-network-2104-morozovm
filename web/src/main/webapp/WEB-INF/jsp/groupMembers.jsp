<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Group members page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/group.css" rel="stylesheet"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container-fluid">
    <div class="row">
        <div class="col text-center">
            <c:if test="${membership}">
                <div class="p-2 bd-highlight">
                    <form action="${pageContext.request.contextPath}/groups/${groupId}/leave" method="post">
                        <input required name="groupId" type="hidden" value="${groupId}">
                        <input required name="userId" type="hidden" value="${authId}">
                        <button type="submit" class="btn btn-success">Leave group</button>
                    </form>
                </div>
            </c:if>
            <c:if test="${!membership}">
                <div class="p-2 bd-highlight">
                    <form action="${pageContext.request.contextPath}/groups/${groupId}/join" method="post">
                        <input required name="groupId" type="hidden" value="${groupId}">
                        <input required name="userId" type="hidden" value="${authId}">
                        <button type="submit" class="btn btn-success">Join group</button>
                    </form>
                </div>
            </c:if>
        </div>
    </div>
</div>
<c:if test="${members.size() == 0}">
    <h5 class="alert-heading text-center">Group does not have any members yet</h5>
</c:if>
<c:if test="${members.size() != 0}">
    <h5 class="alert-heading text-center">Group members</h5>
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-sm-4" id="main">
                <div class="list-group text-center" id="list">
                    <c:forEach var="user" items="${members}">
                        <a href="${pageContext.request.contextPath}/users/${user.id}"
                           class="list-group-item list-group-item-action">${user.surname} ${user.name}</a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <br>
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center d-flex flex-wrap">
            <li class="page-item" id="previousButton" hidden>
                <form id="previousForm">
                    <input name="groupId" type="hidden" value="${groupId}">
                    <input type="hidden" name="currentPage" value="${currentPage}" id="currentPagePrev">
                    <input type="hidden" name="numberOfPages" value="${numberOfPages}" id="numberOfPages">
                    <input class="page-link" type="submit" name="page" value="Previous">
                </form>
            </li>
            <li class="page-item active" id="page1">
                <form class="paging">
                    <input name="groupId" type="hidden" value="${groupId}">
                    <input type="hidden" name="currentPage" value="1">
                    <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                    <input class="page-link" type="submit" name="page" value="1">
                </form>
            </li>
            <c:if test="${numberOfPages > 1}">
                <c:forEach begin="2" end="${numberOfPages}" var="i">
                    <li class="page-item" id="page${i}">
                        <form class="paging">
                            <input name="groupId" type="hidden" value="${groupId}">
                            <input type="hidden" name="currentPage" value="${i}">
                            <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                            <input class="page-link" type="submit" name="page" value="${i}">
                        </form>
                    </li>
                </c:forEach>
            </c:if>
            <c:if test="${currentPage lt numberOfPages}">
                <li class="page-item" id="nextButton">
                    <form id="nextForm">
                        <input name="groupId" type="hidden" value="${groupId}">
                        <input type="hidden" name="currentPage" value="${currentPage}" id="currentPageNext">
                        <input type="hidden" name="numberOfPages" value="${numberOfPages}" id="numberOfPages">
                        <input class="page-link" type="submit" name="page" value="Next">
                    </form>
                </li>
            </c:if>
        </ul>
    </nav>
</c:if>

<script src="${pageContext.request.contextPath}/js/pageNavigationGroupMembers.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>
</html>
