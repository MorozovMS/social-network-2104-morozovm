<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Update user password</title>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container-fluid">
    <div class="welcomeText">Please, enter password:</div>
</div>
<br>
<form action="${pageContext.request.contextPath}/users/${authId}/password" method="post">
    <div class="container-fluid">
        <div class="row">
            <div class="col-auto">
                <label for="userPassword" class="form-label">Your password</label>
                <input required name="password" type="password" class="form-control" id="userPassword">
            </div>
        </div>
    </div>
    <br>
    <div class="container-fluid">
        <input name="btn" class="btn btn-primary" type="submit" value="Change password">
    </div>
</form>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>

</body>
</html>
