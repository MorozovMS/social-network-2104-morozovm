<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Update group</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>

<c:if test="${checkImage}">
    <img src="${pageContext.request.contextPath}/groups/${group.id}/image/" alt="Фото"
         class="img-thumbnail" width="400" height="400">
    <br><br>
    <div class="container-fluid">
        <form class="row g-3" action="${pageContext.request.contextPath}/groups/${group.id}/image/update"
              method="post"
              enctype="multipart/form-data">
            <div class="fw-light">The photo size should be less than 65 kb</div>
            <div class="col-auto">
                <div class="mb-3">
                    <input required class="form-control" type="file" name="file" onchange="checkPhotoSize(this)">
                </div>
            </div>
            <input name="id" type="hidden" value="${group.id}">
            <input name="creatorId" type="hidden" value="${group.creator.id}">
            <div class="col-auto">
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="Update photo">
                </div>
            </div>
        </form>
    </div>
    <div class="col-auto">
        <form action="${pageContext.request.contextPath}/groups/${group.id}/image/delete" method="post">
            <input name="creatorId" type="hidden" value="${group.creator.id}">
            <input class="btn btn-primary" type="submit" value="Delete photo">
        </form>
    </div>
</c:if>

<c:if test="${!checkImage}">
    <div class="container-fluid">
        <form class="row g-3" action="${pageContext.request.contextPath}/groups/${group.id}/image"
              method="post"
              enctype="multipart/form-data">
            <div class="fw-light">The photo size should be less than 65 kb</div>
            <div class="col-auto">
                <div class="mb-3">
                    <input required class="form-control" type="file" name="file" onchange="checkPhotoSize(this)">
                </div>
            </div>
            <input name="id" type="hidden" value="${group.id}">
            <input name="creatorId" type="hidden" value="${group.creator.id}">
            <div class="col-auto">
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="Download photo">
                </div>
            </div>
        </form>
    </div>
</c:if>

<form action="${pageContext.request.contextPath}/groups/${group.id}" method="post"
      onSubmit="return confirm('Are you sure?');">
    <input name="id" type="hidden" value="${group.id}">
    <input name="creatorId" type="hidden" value="${group.creator.id}">
    <input name="createDate" type="hidden" value="${group.creationDate}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-auto">
                <label for="groupName" class="form-label">Group name</label>
                <input required name="name" type="text" class="form-control" id="groupName" value="${group.name}">
            </div>
        </div>
    </div>
    <br>
    <div class="col-sm-3">
        <div class="form-floating">
            <textarea name="description" class="form-control" id="description"
                      style="height: 140px">${group.description}</textarea>
            <label for="description">Group description</label>
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Save changes</button>
</form>

<form action="${pageContext.request.contextPath}/groups/${group.id}/delete" method="post"
      onSubmit="return confirm('Are you sure? This operation cannot be undone');">
    <input name="id" type="hidden" value="${group.id}">
    <input name="creatorId" type="hidden" value="${group.creator.id}">
    <input name="createDate" type="hidden" value="${group.creationDate}">
    <input name="name" type="hidden" value="${group.name}">
    <input name="description" type="hidden" value="${group.description}">
    <button type="submit" class="btn btn-danger">Delete group</button>
</form>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/js/checkPhotoSize.js"></script>
</body>
</html>
