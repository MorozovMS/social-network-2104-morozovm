<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Social network</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/index.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body>
<header>
    <a href="${pageContext.request.contextPath}/" class="logo"><img
            src="${pageContext.request.contextPath}/img/logo.png" alt="logo"></a>
</header>
<c:if test="${checkLogPas == false}">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
             class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img"
             aria-label="Warning:">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
        </svg>
        <h5 class="alert-heading">Incorrect login or password.</h5>
        <h6 class="alert-heading">Please, try again.</h6>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>
<h5 class="alert-heading text-center">Please, enter your e-mail and password to login</h5>
<form action="${pageContext.request.contextPath}/login" method="post">
    <div class="col-md-2 offset-md-5 text-center">
        <label for="username" class="form-label">Enter email</label>
        <input required name="username" type="email" class="form-control" id="username">
    </div>
    <div class="col-md-2 offset-md-5 text-center">
        <label for="password" class="form-label">Password</label>
        <input required name="password" type="password" class="form-control" id="password">
    </div>
    <br>
    <div class="form-check">
        <label class="form-check-label" for="flexCheckDefault"><input class="form-check-input" name="remember-me"
                                                                      type="checkbox" value="true"
                                                                      id="flexCheckDefault">Remember me</label>
    </div>
    <br>
    <div class="d-grid gap-2 col-2 mx-auto">
        <input class="btn btn-primary btn-lg" type="submit" value="Enter">
    </div>
</form>
<h4 class="alert-heading text-center">You don't have an account? Join us!</h4>
<br>
<div class="d-grid gap-2 col-2 mx-auto">
    <a href="${pageContext.request.contextPath}/register" class="btn btn-primary btn-lg">Register</a>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>
</html>
