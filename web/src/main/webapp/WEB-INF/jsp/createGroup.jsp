<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Create group page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>
<h5 class="alert-heading text-center">Please, enter group information:</h5>
<form class="p-4" action="${pageContext.request.contextPath}/groups/create" method="post"
      onSubmit="return confirm('Are you sure?');">
    <div class="row">
        <div class="col-auto">
            <input required name="creatorId" type="hidden" value="${authId}">
            <label for="groupName" class="form-label">Group name</label>
            <input required name="name" type="text" class="form-control" id="groupName">
        </div>
    </div>
    <br>
    <div class="col-sm-3">
        <div class="form-floating">
                    <textarea name="description" class="form-control" placeholder="Enter information about group..."
                              id="addInfo" style="height: 140px"></textarea>
            <label for="addInfo">Group description</label>
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Create group</button>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>
</html>
