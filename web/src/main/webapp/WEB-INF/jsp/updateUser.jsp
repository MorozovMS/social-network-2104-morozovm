<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Update user</title>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
<jsp:include page="header.jsp"/>

<c:if test="${checkImage}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-10">
                <img src="${pageContext.request.contextPath}/users/${user.id}/image" alt="Фото"
                     class="img-thumbnail" width="400" height="400">
            </div>
            <div class="col-sm-2">
                <div class="row">
                    <div class="p-2 bd-highlight">
                        <form action="${pageContext.request.contextPath}/users/${user.id}/xml/export" method="get">
                            <input type="hidden" required name="id" value="${authId}">
                            <button type="submit" class="btn btn-secondary">Export to XML</button>
                        </form>
                    </div>
                    <div class="p-2 bd-highlight">
                        <form class="row g-3"
                              action="${pageContext.request.contextPath}/users/${user.id}/xml/import" method="post"
                              enctype="multipart/form-data">
                            <div class="col-auto">
                                <div class="mb-3">
                                    <input required class="form-control" type="file" name="file">
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="mb-3">
                                    <input class="btn btn-secondary" type="submit" value="Import from XML">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-10">
                <form class="row g-3"
                      action="${pageContext.request.contextPath}/users/${user.id}/image/update"
                      method="post" enctype="multipart/form-data">
                    <div class="col-auto">
                        <div class="mb-3">
                            <input required class="form-control" type="file" name="file"
                                   onchange="checkPhotoSize(this)">
                        </div>
                    </div>
                    <div class="fw-light">The photo size should be less than 65 kb</div>
                    <div class="col-auto">
                        <div class="mb-3">
                            <input class="btn btn-primary" type="submit" value="Update photo">
                        </div>
                    </div>
                </form>
                <div class="col-auto">
                    <form action="${pageContext.request.contextPath}/users/${user.id}/image/delete"
                          method="post">
                        <input class="btn btn-primary" type="submit" value="Delete photo">
                    </form>
                </div>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${!checkImage}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-10">
                <form class="row g-3" action="${pageContext.request.contextPath}/users/${user.id}/image"
                      method="post" enctype="multipart/form-data">
                    <div class="col-auto">
                        <div class="mb-3">
                            <input required class="form-control" type="file" name="file"
                                   onchange="checkPhotoSize(this)">
                        </div>
                    </div>
                    <div class="fw-light">The photo size should be less than 65 kb</div>
                    <div class="col-auto">
                        <div class="mb-3">
                            <input class="btn btn-primary" type="submit" value="Download photo">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-2">
                <div class="row">
                    <div class="p-2 bd-highlight">
                        <form action="${pageContext.request.contextPath}/users/${user.id}/xml/export" method="get">
                            <input type="hidden" required name="id" value="${authId}">
                            <button type="submit" class="btn btn-secondary">Export to XML</button>
                        </form>
                    </div>
                    <div class="p-2 bd-highlight">
                        <form class="row g-3"
                              action="${pageContext.request.contextPath}/users/${user.id}/xml/import" method="post"
                              enctype="multipart/form-data">
                            <div class="col-auto">
                                <div class="mb-3">
                                    <input required class="form-control" type="file" name="file">
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="mb-3">
                                    <input class="btn btn-secondary" type="submit" value="Import from XML">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>

<div class="container-fluid">
    <form action="${pageContext.request.contextPath}/users/${user.id}/password" method="get">
        <input type="submit" class="btn btn-primary" value="Change password">
    </form>
</div>

<div class="container-fluid">
    <form action="${pageContext.request.contextPath}/users/${user.id}/delete" method="post"
          onSubmit="return confirm('Are you sure? Delete an account?');">
        <input name="btn" id="submitButton" class="btn btn-warning" type="submit" value="Delete account">
    </form>
</div>

<div class="container-fluid">
    <div class="welcomeText">Please, enter updated information:</div>
    <form action="${pageContext.request.contextPath}/users/${user.id}" method="post" name="myForm"
          onSubmit="return confirm('Are you sure?');">
        <input name="id" type="hidden" value="${authId}">
        <div class="container-fluid">
            <div class="row">
                <div class="col-auto">
                    <label for="userName" class="form-label">Your name</label>
                    <input required name="name" type="text" class="form-control" id="userName" value="${user.name}">
                </div>
                <div class="col-auto">
                    <label for="userSurname" class="form-label">Your surname</label>
                    <input required name="surname" type="text" class="form-control" id="userSurname"
                           value="${user.surname}">
                </div>
                <div class="col-auto">
                    <label for="userMidname" class="form-label">Your midname</label>
                    <input name="midName" type="text" class="form-control" id="userMidname" value="${user.midName}">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-auto">
                    <label for="userEmail" class="form-label">Your email</label>
                    <input required name="email" type="email" class="form-control" id="userEmail"
                           value="${user.email}">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <c:set var="sex" scope="request" value="${user.sex}"/>
            <c:if test="${sex == 0}">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault1"
                           value="male">
                    <label class="form-check-label" for="flexRadioDefault1">Male</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault2"
                           value="female"
                           checked>
                    <label class="form-check-label" for="flexRadioDefault2">Female</label>
                </div>
            </c:if>
            <c:if test="${sex == 1}">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault3"
                           value="male"
                           checked>
                    <label class="form-check-label" for="flexRadioDefault3">Male</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault4"
                           value="female">
                    <label class="form-check-label" for="flexRadioDefault4">Female</label>
                </div>
            </c:if>
            <br>
        </div>
        <div class="container-fluid">
            <p>Want to change phone/phones?</p>
            <c:set var="count" value="0"/>
            <div class="row">
                <div class="col-auto" id="phoneInputGroup">
                    <div class="input-group">
                        <input type="tel" class="form-control" id="phoneInput" name="phone" aria-label="phone">
                        <button class="btn btn-outline-secondary" type="button" id="checkButton">Add</button>
                    </div>
                    <c:if test="${user.phones.size() == 0}">
                        <div class="col-auto" id="phonesLoad"></div>
                    </c:if>
                    <c:forEach var="phone" items="${user.phones}">
                        <c:set var="count" value="${count + 1}"/>
                        <div class="col-auto" id="phonesLoad">
                            <div class="input-group" id="phoneGroup${count}">
                                <input readonly type="tel" class="form-control" name="phone${count}" aria-label="phone"
                                       value="${phone.phone}"
                                       id="phone${count}">
                                <input type="hidden" name="phone_id${count}" value="${phone.id}">
                                <button class="btn btn-outline-secondary" type="button" id="delButton${count}"
                                        onclick="deletePhone(${count})">Delete
                                </button>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <input type="hidden" name="phonesCounter" value="${count}" id="phonesCounter">
        <br>
        <div class="container-fluid">
            <p>Your skype:</p>
            <div class="row">
                <div class="col-auto">
                    <label for="skype" class="form-label">Skype</label>
                    <input name="skype" type="text" class="form-control" id="skype" value="${user.skype}">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Your telegram</p>
            <div class="row">
                <div class="col-auto">
                    <label for="telegram" class="form-label">Telegram</label>
                    <input name="telegram" type="text" class="form-control" id="telegram"
                           value="${user.telegram}">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Enter your birth date:</p>
            <input name="bDay" type="date" value="${user.BDay}" aria-label="bDay">
            <br><br>
            <p>Enter your home and work addresses:</p>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-floating">
                <textarea name="homeAddress" class="form-control" id="homeAddress"
                          style="height: 100px">${user.homeAddress}</textarea>
                        <label for="homeAddress">Home address</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-floating">
                <textarea name="workAddress" class="form-control" id="workAddress"
                          style="height: 100px">${user.workAddress}</textarea>
                        <label for="workAddress">Work address</label>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <div class="container-fluid">
            <p>Enter additional information:</p>
            <div class="col-sm-5">
                <div class="form-floating">
            <textarea name="addInfo" class="form-control" id="addInfo"
                      style="height: 140px">${user.addInfo}</textarea>
                    <label for="addInfo">Additional information</label>
                </div>
            </div>
        </div>
        <br><br>
        <div class="container-fluid">
            <input name="btn" id="submitButton" class="btn btn-primary" type="submit" value="Save changes">
        </div>
    </form>
    <br>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/js/editPhones.js"></script>
<script src="${pageContext.request.contextPath}/js/checkPhotoSize.js"></script>

</body>
</html>
