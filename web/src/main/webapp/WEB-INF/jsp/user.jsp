<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>User page</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="text-end fw-light">You are logged in as ${authName} ${authSurname}</div>
<div class="userName">${user.surname} ${user.name} ${user.midName}</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <c:if test="${checkImage}">
                <img src="${pageContext.request.contextPath}/users/${user.id}/image" alt="Фото"
                     class="img-thumbnail" width="400" height="400">
            </c:if>
            <br>
            <p class="fw-bold"><img src="${pageContext.request.contextPath}/img/bootstrapIcons/info-circle.svg"
                                    alt="circle-icon"> Contact info:</p>
            <c:set var="skype" scope="request" value="${user.skype}"/>
            <c:choose>
                <c:when test="${skype == null}"> </c:when>
                <c:when test="${skype == \"\"}"> </c:when>
                <c:otherwise><p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/skype.svg"
                                     alt="skype-icon">
                    Skype: <c:out value="${skype}"/></p>
                </c:otherwise>
            </c:choose>

            <c:set var="telegram" scope="request" value="${user.telegram}"/>
            <c:choose>
                <c:when test="${telegram == null}"> </c:when>
                <c:when test="${telegram == \"\"}"> </c:when>
                <c:otherwise><p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/telegram.svg"
                                     alt="telegram-icon">
                    Telegram: <c:out value="${telegram}"/></p>
                </c:otherwise>
            </c:choose>

            <p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/mailbox2.svg" alt="mail-icon">
                e-mail: ${user.email}</p>

            <c:if test="${userPhones != null}">
                <c:forEach var="phone" items="${userPhones}">
                    <p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/telephone.svg" alt="phone-icon">
                        Phone: ${phone.phone}</p>
                </c:forEach>
            </c:if>

            <c:set var="BDay" scope="request" value="${user.BDay}"/>
            <c:choose>
                <c:when test="${BDay == null}"> </c:when>
                <c:when test="${BDay == \"\"}"> </c:when>
                <c:otherwise><p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/calendar2-check.svg"
                                     alt="calendar-icon">
                    Birth date: <c:out value="${BDay}"/></p>
                </c:otherwise>
            </c:choose>

            <c:set var="sex" scope="request" value="${user.sex}"/>
            <c:if test="${sex == 0}">
                <p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/gender-female.svg" alt="gender-icon">
                    Sex: female</p>
            </c:if>
            <c:if test="${sex == 1}">
                <p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/gender-male.svg" alt="gender-icon">
                    Sex: male </p>
            </c:if>
            <c:set var="homeAddress" scope="request" value="${user.homeAddress}"/>
            <c:choose>
                <c:when test="${homeAddress == null}"> </c:when>
                <c:when test="${homeAddress == \"\"}"> </c:when>
                <c:otherwise><p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/house.svg"
                                     alt="house-icon">
                    Home address: <c:out value="${homeAddress}"/></p>
                </c:otherwise>
            </c:choose>
            <c:set var="workAddress" scope="request" value="${user.workAddress}"/>
            <c:choose>
                <c:when test="${workAddress == null}"> </c:when>
                <c:when test="${workAddress == \"\"}"> </c:when>
                <c:otherwise><p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/house-door-fill.svg"
                                     alt="house-door-icon">Work address: <c:out value="${workAddress}"/></p>
                </c:otherwise>
            </c:choose>
            <c:set var="addInfo" scope="request" value="${user.addInfo}"/>
            <c:choose>
                <c:when test="${addInfo == null}"> </c:when>
                <c:when test="${addInfo == \"\"}"> </c:when>
                <c:otherwise><p><img src="${pageContext.request.contextPath}/img/bootstrapIcons/info-lg.svg"
                                     alt="info-icon">
                    Additional information: <c:out value="${addInfo}"/></p>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-sm-2">
            <form action="${pageContext.request.contextPath}/users/${user.id}/user-groups" method="get">
                <button class="btn btn-secondary" type="submit">User groups</button>
            </form>
            <form action="${pageContext.request.contextPath}/users/${user.id}/friends" method="get">
                <button class="btn btn-secondary" type="submit">User friends</button>
            </form>
            <c:if test="${checkFriendship}">
                <form action="${pageContext.request.contextPath}/users/${user.id}/friends/delete" method="post">
                    <input required name="friendId" type="hidden" value="${user.id}">
                    <button class="btn btn-secondary" type="submit">Delete from friends</button>
                </form>
            </c:if>
            <c:if test="${checkFriendship == false}">
                <c:if test="${checkFriendshipRequest == false}">
                    <form action="${pageContext.request.contextPath}/users/${user.id}/friends/request" method="post">
                        <input required name="friendId" type="hidden" value="${user.id}">
                        <button class="btn btn-secondary" type="submit">Add to friends</button>
                    </form>
                </c:if>
                <c:if test="${checkFriendshipRequest == true}">
                    <form>
                        <div class="badge bg-secondary">Request sent</div>
                    </form>
                </c:if>
            </c:if>
            <c:if test="${chatTo == true}">
                <a class="btn btn-secondary"
                   href="${pageContext.request.contextPath}/users/${authId}/chat?to=${user.id}">Send message</a>
            </c:if>
            <c:if test="${chatTo == false}">
                <a class="btn btn-secondary"
                   href="${pageContext.request.contextPath}/users/${user.id}/chats">Open chats</a>
            </c:if>
        </div>
    </div>
</div>
<div class="wall"><img src="${pageContext.request.contextPath}/img/bootstrapIcons/bricks.svg" alt="bricks-icon">
    User wall
</div>
<hr>
<br>
<form class="wall" action="${pageContext.request.contextPath}/users/${user.id}/messages/message" method="post">
    <input required name="user_id" type="hidden" value="${user.id}">
    <input required name="creator_id" type="hidden" value="${authId}">
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-sm-4">
                <div class="input-group">
                    <input required name="message" type="text" class="form-control" aria-label="message"
                           placeholder="Enter message...">
                    <button class="btn btn-outline-secondary" type="submit">Send message</button>
                </div>
            </div>
        </div>
    </div>
</form>
<c:forEach var="message" items="${messages}">
    <form class="wall">
        <p>Message: ${message.message}</p>
        <input required name="id" type="hidden" value="${message.id}">
        <input required name="user_id" type="hidden" value="${user.id}">
        <input required name="creator_id" type="hidden" value="${message.creator.id}">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">
                <div class="col-sm-4">
                    <div class="input-group">
                        <input required name="message" type="text" value="${message.message}" class="form-control"
                               aria-label="message">
                        <button class="btn btn-outline-secondary btn-sm"
                                formaction="${pageContext.request.contextPath}/users/${user.id}/messages/message/update"
                                formmethod="post"
                                type="submit">Update message
                        </button>
                        <button class="btn btn-outline-secondary btn-sm"
                                formaction="${pageContext.request.contextPath}/users/${user.id}/messages/message/delete"
                                formmethod="post"
                                type="submit">Delete message
                        </button>
                    </div>
                </div>
                <div class="fs-6 fw-light">
                    Send by: <a href="${pageContext.request.contextPath}/users/${message.creator.id}">
                        ${message.creator.surname} ${message.creator.name}</a></div>
                <div class="fs-6 fw-light">At date: ${message.createdDate}</div>
            </div>
        </div>
    </form>
</c:forEach>

</body>
</html>
