<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/group.css" rel="stylesheet"/>
    <title>Chat</title>
</head>

<jsp:include page="header.jsp"/>

<body>
<div class="container d-none">
    <form>
        <input type="hidden" name="senderId" value="${authId}" id="senderId">
        <input type="hidden" name="recipientId" value="${recipientId}" id="recipientId">
        <input type="hidden" name="senderName" value="${senderName}" id="senderName">
        <input type="hidden" name="recipientName" value="${recipientName}" id="recipientName">
        <input type="hidden" name="chatId" value="${chatId}" id="chatId">
    </form>
</div>

<div id="chatPage" class="container">
    <div class="chat-header">
        <h2>Chat with ${recipientName}</h2>
    </div>
    <div class="card h-50">
        <div class="card-body overflow-auto" id="scroll">
            <div class="list-group text-start" id="list">
                <c:forEach var="message" items="${messages}">
                    <div>
                        <p class="text-break">
                            <span class="fw-light fs-6 fst-italic">
                                Time: ${message.timestamp.hours}:${message.timestamp.minutes}
                            </span>
                            <c:if test="${senderName == message.sender.name}">
                                <span class="badge bg-secondary fs-5">${senderName}:</span>
                            </c:if>
                            <c:if test="${recipientName == message.sender.name}">
                                <span class="badge bg-secondary fs-5">${recipientName}:</span>
                            </c:if>
                            <span>${message.content}</span>
                        </p>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <form id="messagebox" name="messagebox">
        <div class="form-group">
            <br>
            <input type="text" class="form-control" id="message" aria-describedby="name"
                   placeholder="Enter message..." aria-label="message">
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Send</button>
    </form>
</div>
<br>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.3.0/sockjs.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
<script src="${pageContext.request.contextPath}/js/chat.js"></script>
</body>
</html>
