<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>User page</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<header>
    <div class="d-flex bd-highlight mb-3">
        <div class="p-2 bd-highlight">
            <a href="${pageContext.request.contextPath}/users/${authId}"><img
                    src="${pageContext.request.contextPath}/img/logo.png" alt="logo"></a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users/${authId}">My profile</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users/${authId}/news">My news</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users/${authId}/edit">Edit user</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users">All users</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/groups">All groups</a>
            <a class="btn btn-warning ms-3" href="${pageContext.request.contextPath}/logout">Logout</a>
            <div class="d-grid gap-3 d-md-flex justify-content-md-end">
                <c:if test="${isAdmin}">
                    <a class="btn btn-dark btn-lg" href="${pageContext.request.contextPath}/admin">Admin panel</a>
                </c:if>
            </div>
        </div>
        <div class="ms-auto p-2 bd-highlight">
            <form action="${pageContext.request.contextPath}/search" method="get">
                <div class="input-group">
                    <input name="searchQuery" class="form-control me-2" type="search" placeholder="Search"
                           aria-label="Search" id="search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </div>
            </form>
        </div>
    </div>
</header>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/searchAutocomplete.js" charset="UTF-8"></script>
<script>let ctx = "${pageContext.request.contextPath}"</script>
</body>
</html>
