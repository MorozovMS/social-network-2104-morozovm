<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Admin page</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="wall">
    <p class="fw-bold fs-4">Welcome to admin panel</p>
</div>
<br>

<c:if test="${error == true}">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
             class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img"
             aria-label="Warning:">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
        </svg>
        <h5 class="alert-heading">Incorrect id</h5>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>

<c:if test="${success == true}">
    <div class="alert alert-success alert-dismissible text-center" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
             class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img"
             aria-label="Warning:">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"></path>
        </svg>
        <h5 class="alert-heading">Success</h5>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <p class="fw-normal">Set user to read-only mode</p>
            <form action="${pageContext.request.contextPath}/admin/users/read-only" method="post">
                <div class="row">
                    <div class="col-auto">
                        <label for="idSet" class="form-label">Enter user id:</label>
                        <input required name="id" type="number" class="form-control" id="idSet">
                    </div>
                </div>
                <br>
                <input name="btn" class="btn btn-primary" type="submit" value="Set read-only">
            </form>
        </div>
        <div class="col-sm-6">
            <p class="fw-normal">Remove read-only mode for user</p>
            <form action="${pageContext.request.contextPath}/admin/users/read-only/remove" method="post">
                <div class="row">
                    <div class="col-auto">
                        <label for="idRem" class="form-label">Enter user id:</label>
                        <input required name="id" type="number" class="form-control" id="idRem">
                    </div>
                </div>
                <br>
                <input name="btn" class="btn btn-primary" type="submit" value="Remove read-only">
            </form>
        </div>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <p class="fw-normal">Set user status - BANNED</p>
            <form action="${pageContext.request.contextPath}/admin/users/ban" method="post">
                <div class="row">
                    <div class="col-auto">
                        <label for="idBan" class="form-label">Enter user id:</label>
                        <input required name="id" type="number" class="form-control" id="idBan">
                    </div>
                </div>
                <br>
                <input name="btn" class="btn btn-primary" type="submit" value="BAN">
            </form>
        </div>
        <div class="col-sm-6">
            <p class="fw-normal">Set user status - ACTIVE</p>
            <form action="${pageContext.request.contextPath}/admin/users/ban/remove" method="post">
                <div class="row">
                    <div class="col-auto">
                        <label for="idUnban" class="form-label">Enter user id:</label>
                        <input required name="id" type="number" class="form-control" id="idUnban">
                    </div>
                </div>
                <br>
                <input name="btn" class="btn btn-primary" type="submit" value="UNBAN">
            </form>
        </div>
    </div>
</div>
<br>
<div class="container-fluid">
    <p class="fw-normal">Remove user</p>
    <form action="${pageContext.request.contextPath}/admin/users/remove" method="post">
        <div class="row">
            <div class="col-auto">
                <label for="id" class="form-label">Enter user id:</label>
                <input required name="id" type="number" class="form-control" id="id">
            </div>
        </div>
        <br>
        <input name="btn" class="btn btn-primary" type="submit" value="Remove user">
    </form>
</div>
</body>
</html>
