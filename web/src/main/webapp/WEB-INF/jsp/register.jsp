<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Register</title>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/register.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body>
<header>
    <a href="${pageContext.request.contextPath}/" class="logo"><img
            src="${pageContext.request.contextPath}/img/logo.png" alt="logo"></a>
</header>
<c:if test="${checkEmail == false}">
    <div class="alert alert-danger alert-dismissible text-center" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
             class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img"
             aria-label="Warning:">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
        </svg>
        <h5 class="alert-heading">Email already exists.</h5>
        <h6 class="alert-heading">Please, enter another email.</h6>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>
<div class="container-fluid">
    <div class="welcomeText">Please, enter your personal information:</div>
    <br>
    <form action="${pageContext.request.contextPath}/register" method="post" name="myForm"
          onSubmit="return confirm('Are you sure?');">
        <div class="row">
            <div class="col-auto">
                <label for="userName" class="form-label">Your name</label>
                <input required name="name" type="text" class="form-control" id="userName">
            </div>
            <div class="col-auto">
                <label for="userSurname" class="form-label">Your surname</label>
                <input required name="surname" type="text" class="form-control" id="userSurname">
            </div>
            <div class="col-auto">
                <label for="userMidname" class="form-label">Your midname</label>
                <input name="midName" type="text" class="form-control" id="userMidname">
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Your e-mail and password (this information will be used for login):</p>
            <div class="row">
                <div class="col-auto">
                    <label for="userEmail" class="form-label">Your email</label>
                    <input required name="email" type="email" class="form-control" id="userEmail">
                </div>
                <div class="col-auto">
                    <label for="userPassword" class="form-label">Your password</label>
                    <input required name="password" type="password" class="form-control" id="userPassword">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Select your gender:</p>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault1" value="male" checked>
                <label class="form-check-label" for="flexRadioDefault1">Male</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault2" value="female">
                <label class="form-check-label" for="flexRadioDefault2">Female</label>
            </div>
            <br>
            <p>Enter your phone/phones:</p>
            <input type="hidden" name="phonesCounter" value="0" id="phonesCounter">
            <div class="row">
                <div class="col-auto" id="phoneInputGroup">
                    <div class="input-group">
                        <input type="tel" class="form-control" id="phoneInput" name="phone" aria-label="phone">
                        <button class="btn btn-outline-secondary" type="button" id="checkButton">Add</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Your skype:</p>
            <div class="row">
                <div class="col-auto">
                    <label for="skype" class="form-label">Skype</label>
                    <input name="skype" type="text" class="form-control" id="skype">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Your telegram</p>
            <div class="row">
                <div class="col-auto">
                    <label for="telegram" class="form-label">Telegram</label>
                    <input name="telegram" type="text" class="form-control" id="telegram">
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <p>Enter your birth date:</p>
            <input required name="bDay" type="date" aria-label="bDay"><br>
            <br>
            <p>Enter your home and work addresses:</p>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-floating">
                <textarea name="homeAddress" class="form-control" placeholder="Enter your home address..."
                          id="homeAddress" style="height: 100px"></textarea>
                        <label for="homeAddress">Home address</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-floating">
                <textarea name="workAddress" class="form-control" placeholder="Enter your work address..."
                          id="workAddress" style="height: 100px"></textarea>
                        <label for="workAddress">Work address</label>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <div class="container-fluid">
            <p>Enter additional information:</p>
            <div class="col-sm-5">
                <div class="form-floating">
                <textarea name="addInfo" class="form-control" placeholder="Enter additional information..."
                          id="addInfo" style="height: 140px"></textarea>
                    <label for="addInfo">Additional information</label>
                </div>
            </div>
            <br><br>
            <button type="submit" class="btn btn-primary">Register</button>
        </div>
    </form>
</div>

<script src="${pageContext.request.contextPath}/js/checkPhones.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>

</body>
</html>

