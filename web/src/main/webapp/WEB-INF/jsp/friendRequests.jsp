<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Friend requests</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/group.css" rel="stylesheet"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <div class="row justify-content-center">
                <div class="col-sm-4">
                    <div class="badge bg-primary">Input requests</div>
                </div>
            </div>
            <br>
            <table class="table-primary table-hover">
                <tbody>
                <c:forEach var="input" items="${inputRequests}">
                    <tr>
                        <td>
                            <a href="${pageContext.request.contextPath}/users/${input.id}"
                               class="link-primary">
                                    ${input.surname} ${input.name} ${input.midName} </a>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/users/${authId}/friends/requests/accept"
                                  method="post">
                                <input required type="hidden" name="friendId" value="${input.id}">
                                <button class="btn btn-outline-success btn-sm" type="submit">Accept</button>
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/users/${authId}/friends/requests/decline"
                                  method="post">
                                <input required type="hidden" name="friendId" value="${input.id}">
                                <button class="btn btn-outline-success btn-sm" type="submit">Decline</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-sm-6">
            <div class="row justify-content-center">
                <div class="col-sm-4">
                    <div class="badge bg-primary">Outgoing requests</div>
                </div>
            </div>
            <br>
            <table class="table-primary table-hover">
                <tbody>
                <c:forEach var="outgoing" items="${outgoingRequests}">
                    <tr>
                        <td>
                            <a href="${pageContext.request.contextPath}/users/${outgoing.id}" class="link-primary">
                                    ${outgoing.surname} ${outgoing.name} ${outgoing.midName} </a>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/users/${authId}/friends/requests/delete"
                                  method="post">
                                <input required type="hidden" name="id" value="${authId}">
                                <input required type="hidden" name="friendId" value="${outgoing.id}">
                                <button class="btn btn-outline-success btn-sm" type="submit">Cancel</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script src="${pageContext.request.contextPath}/js/pageNavigationUserGroups.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>
</html>
