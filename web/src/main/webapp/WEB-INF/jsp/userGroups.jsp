<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>User groups page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/group.css" rel="stylesheet"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="d-flex justify-content-center">
    <a href="${pageContext.request.contextPath}/groups/create" class="btn btn-success btn-lg">
        Create group</a>
</div>
<br>
<c:if test="${groups.size() == 0}">
    <h5 class="alert-heading text-center">User does not have any groups yet</h5>
</c:if>
<c:if test="${groups.size() != 0}">
    <h5 class="alert-heading text-center">User groups</h5>
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-sm-4" id="main">
                <div class="list-group text-center" id="list">
                    <c:forEach var="group" items="${groups}">
                        <a href="${pageContext.request.contextPath}/groups/${group.id}"
                           class="list-group-item list-group-item-action">${group.name}</a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <br>
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center d-flex flex-wrap">
            <li class="page-item" id="previousButton" hidden>
                <form id="previousForm">
                    <input type="hidden" name="id" value="${id}">
                    <input type="hidden" name="currentPage" value="${currentPage}" id="currentPagePrev">
                    <input type="hidden" name="numberOfPages" value="${numberOfPages}" id="numberOfPages">
                    <input class="page-link" type="submit" name="page" value="Previous">
                </form>
            </li>
            <li class="page-item active" id="page1">
                <form class="paging">
                    <input type="hidden" name="id" value="${id}">
                    <input type="hidden" name="currentPage" value="1">
                    <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                    <input class="page-link" type="submit" name="page" value="1">
                </form>
            </li>
            <c:if test="${numberOfPages > 1}">
                <c:forEach begin="2" end="${numberOfPages}" var="i">
                    <li class="page-item" id="page${i}">
                        <form class="paging">
                            <input type="hidden" name="id" value="${id}">
                            <input type="hidden" name="currentPage" value="${i}">
                            <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                            <input class="page-link" type="submit" name="page" value="${i}">
                        </form>
                    </li>
                </c:forEach>
            </c:if>
            <c:if test="${currentPage lt numberOfPages}">
                <li class="page-item" id="nextButton">
                    <form id="nextForm">
                        <input type="hidden" name="id" value="${id}">
                        <input type="hidden" name="currentPage" value="${currentPage}" id="currentPageNext">
                        <input type="hidden" name="numberOfPages" value="${numberOfPages}" id="numberOfPages">
                        <input class="page-link" type="submit" name="page" value="Next">
                    </form>
                </li>
            </c:if>
        </ul>
    </nav>
</c:if>

<script src="${pageContext.request.contextPath}/js/pageNavigationUserGroups.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>
</html>
