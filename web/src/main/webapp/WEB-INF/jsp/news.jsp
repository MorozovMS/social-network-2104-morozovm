<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>User news</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="text-end fw-light">You are logged in as ${authName} ${authSurname}</div>
<div class="userName">${user.surname} ${user.name} ${user.midName}</div>

<div class="wall"><img src="${pageContext.request.contextPath}/img/bootstrapIcons/bricks.svg" alt="bricks-icon">
    News
</div>
<hr>
<br>
<form class="wall" action="${pageContext.request.contextPath}/users/${authId}/messages/message" method="post">
    <input required name="user_id" type="hidden" value="${authId}">
    <input required name="creator_id" type="hidden" value="${authId}">
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-sm-4">
                <div class="input-group">
                    <input required name="message" type="text" class="form-control" aria-label="message"
                           placeholder="Enter message...">
                    <button class="btn btn-outline-secondary" type="submit">Send message</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="container-fluid">
    <div class="row justify-content-center align-items-center">
        <div class="col-sm-8" id="main">
            <div class="list-group text-center" id="list">
                <c:forEach var="message" items="${news}">
                    <p class="list-group-item list-group-item-action">Message from <a
                            href="${pageContext.request.contextPath}/users/${message.creatorId}">${message.creatorName}</a>
                        at ${message.createdDate}: ${message.message}</p>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

<br>
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center d-flex flex-wrap">
        <li class="page-item" id="previousButton" hidden>
            <form id="previousForm">
                <input type="hidden" name="id" value="${authId}" id="id">
                <input type="hidden" name="currentPage" value="${currentPage}" id="currentPagePrev">
                <input type="hidden" name="numberOfPages" value="${numberOfPages}" id="numberOfPages">
                <input class="page-link" type="submit" name="page" value="Previous">
            </form>
        </li>
        <li class="page-item active" id="page1">
            <form class="paging">
                <input type="hidden" name="id" value="${authId}" id="id">
                <input type="hidden" name="currentPage" value="1">
                <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                <input class="page-link" type="submit" name="page" value="1">
            </form>
        </li>
        <c:if test="${numberOfPages > 1}">
            <c:forEach begin="2" end="${numberOfPages}" var="i">
                <li class="page-item" id="page${i}">
                    <form class="paging">
                        <input type="hidden" name="id" value="${authId}" id="id">
                        <input type="hidden" name="currentPage" value="${i}">
                        <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                        <input class="page-link" type="submit" name="page" value="${i}">
                    </form>
                </li>
            </c:forEach>
        </c:if>
        <c:if test="${currentPage lt numberOfPages}">
            <li class="page-item" id="nextButton">
                <form id="nextForm">
                    <input type="hidden" name="id" value="${authId}" id="id">
                    <input type="hidden" name="currentPage" value="${currentPage}" id="currentPageNext">
                    <input type="hidden" name="numberOfPages" value="${numberOfPages}" id="numberOfPages">
                    <input class="page-link" type="submit" name="page" value="Next">
                </form>
            </li>
        </c:if>
    </ul>
</nav>
<script src="${pageContext.request.contextPath}/js/pageNavigationNews.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
</body>
</html>
