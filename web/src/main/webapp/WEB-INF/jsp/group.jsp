<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta charset="utf-8"/>
    <title>Group page</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link type="text/css" href="${pageContext.request.contextPath}/css/group.css" rel="stylesheet"/>
    <link type="text/css" href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body>
<header>
    <div class="d-flex bd-highlight mb-3">
        <div class="p-2 bd-highlight">
            <a href="${pageContext.request.contextPath}/users/${authId}"><img
                    src="${pageContext.request.contextPath}/img/logo.png" alt="logo"></a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users/${authId}">My profile</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users/${authId}/news">My news</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users/${authId}/edit">Edit
                user</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/groups/${group.id}/edit">Edit group</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/users">All users</a>
            <a class="btn btn-primary ms-3" aria-current="page"
               href="${pageContext.request.contextPath}/groups">All groups</a>
            <a class="btn btn-warning ms-3" href="${pageContext.request.contextPath}/logout">Logout</a>
            <div class="d-grid gap-3 d-md-flex justify-content-md-end">
                <c:if test="${isAdmin}">
                    <a class="btn btn-dark btn-lg" href="${pageContext.request.contextPath}/admin">Admin panel</a>
                </c:if>
            </div>
        </div>
        <div class="ms-auto p-2 bd-highlight">
            <form action="${pageContext.request.contextPath}/search" method="get">
                <div class="input-group">
                    <input name="searchQuery" class="form-control me-2" type="search" placeholder="Search"
                           aria-label="Search" id="search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </div>
            </form>
        </div>
    </div>
</header>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <c:if test="${checkImage}">
                <img src="${pageContext.request.contextPath}/groups/${group.id}/image" alt="Фото"
                     class="img-thumbnail" width="400" height="400">
            </c:if>
            <div class="welcomeText">
                <p>Group name: ${group.name}</p>
                <p>Group description: ${group.description}</p>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="row">
                <form action="${pageContext.request.contextPath}/groups/${group.id}/members" method="get">
                    <input required name="id" type="hidden" value="${group.id}">
                    <button class="btn btn-secondary" type="submit">Group members</button>
                </form>
            </div>
            <div class="row">
                <c:if test="${membership}">
                    <div class="p-2 bd-highlight">
                        <form action="${pageContext.request.contextPath}/groups/${group.id}/leave" method="post">
                            <input required name="groupId" type="hidden" value="${group.id}">
                            <input required name="userId" type="hidden" value="${authId}">
                            <button type="submit" class="btn btn-success">Leave group</button>
                        </form>
                    </div>
                </c:if>
                <c:if test="${!membership}">
                    <div class="p-2 bd-highlight">
                        <form action="${pageContext.request.contextPath}/groups/${group.id}/join" method="post">
                            <input required name="groupId" type="hidden" value="${group.id}">
                            <input required name="userId" type="hidden" value="${authId}">
                            <button type="submit" class="btn btn-success">Join group</button>
                        </form>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>

<div class="wall"><img src="${pageContext.request.contextPath}/img/bootstrapIcons/bricks.svg" alt="Group wall icon">
    Group wall
</div>
<hr>
<br>
<form class="wall" action="${pageContext.request.contextPath}/groups/${group.id}/messages/message" method="post">
    <input required name="creatorId" type="hidden" value="${authId}">
    <input required name="groupId" type="hidden" value="${group.id}">
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-sm-4">
                <div class="input-group">
                    <input required name="message" type="text" class="form-control" aria-label="message"
                           placeholder="Enter message...">
                    <button class="btn btn-outline-secondary" type="submit">Send message</button>
                </div>
            </div>
        </div>
    </div>
</form>

<c:forEach var="message" items="${messages}">
    <form class="wall">
        <p>Message: ${message.message}</p>
        <input required name="groupId" type="hidden" value="${group.id}">
        <input required name="id" type="hidden" value="${message.id}">
        <input required name="creatorId" type="hidden" value="${message.creator.id}">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">
                <div class="col-sm-4">
                    <div class="input-group">
                        <input required name="message" type="text" value="${message.message}" aria-label="message"
                               class="form-control">
                        <button class="btn btn-outline-secondary btn-sm"
                                formaction="${pageContext.request.contextPath}/groups/${message.group.id}/messages/message/update"
                                formmethod="post"
                                type="submit">Update message
                        </button>
                        <button class="btn btn-outline-secondary btn-sm"
                                formaction="${pageContext.request.contextPath}/groups/${message.group.id}/messages/message/delete"
                                formmethod="post"
                                type="submit">Delete message
                        </button>
                    </div>
                    <div class="fs-6 fw-light">
                        Send by: <a href="${pageContext.request.contextPath}/users/${message.creator.id}">
                            ${message.creator.surname} ${message.creator.name}</a></div>
                    <div class="fs-6 fw-light">At date: ${message.createdDate}</div>
                </div>
            </div>
        </div>
    </form>
</c:forEach>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/js/searchAutocomplete.js" charset="UTF-8"></script>
<script>let ctx = "${pageContext.request.contextPath}"</script>
</body>
</html>
