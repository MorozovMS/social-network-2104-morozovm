function validatePhoneNumber() {
    let x = document.forms["myForm"]["phone"].value;
    let regex = /^[]?[(]?[0-9]{3}[)]?[-\s]?[0-9]{3}[-\s]?[0-9]{4,6}$/im;
    return regex.test(x);
}

checkButton.onclick = function () {
    if (validatePhoneNumber()) {
        let phonesCounter = document.getElementById('phonesCounter').value
        document.getElementById('phonesCounter').value = ++phonesCounter
        let input = document.createElement("input")
        input.type = 'tel'
        input.className = 'form-control'
        input.name = 'phone' + phonesCounter
        input.setAttribute('value', document.forms["myForm"]["phone"].value)
        input.setAttribute('readonly', 'readonly')
        let inputHidden = document.createElement("input")
        inputHidden.type = 'text'
        inputHidden.name = 'phone_id' + phonesCounter
        inputHidden.setAttribute('hidden', 'hidden')
        inputHidden.setAttribute('value', '0')
        let divInput = document.createElement('div')
        divInput.className = 'input-group'
        divInput.appendChild(input);
        divInput.appendChild(inputHidden)
        let delButton = document.createElement("button")
        delButton.className = "btn btn-outline-secondary"
        delButton.type = "button"
        delButton.textContent = "Delete"
        delButton.onclick = function () {
            input.value = ''
            divInput.remove()
        }
        divInput.appendChild(delButton)
        document.getElementById('phonesLoad').appendChild(divInput)
        document.getElementById('phoneInput').value = ''
    } else {
        alert('Wrong phone number')
    }
}

function deletePhone(i) {
    document.getElementById('phone' + i).value = ''
    document.getElementById('phoneGroup' + i).remove()
}
