function checkPhotoSize(file) {
    const fileSize = file.files[0].size / 1024
    if (fileSize > 64) {
        alert('Too big file. Limit is 65 kb');
        $(file).val('');
    }
}