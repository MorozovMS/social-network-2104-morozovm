$(".paging").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/search/page/' + formData.get("currentPage") +
        '?searchQuery=' + formData.get("searchQuery")),
        function (data) {
            fillUsersList(data);
        })
    setActivePage(formData.get("currentPage"));
    checkNexPrevButtons();
});

$("#nextForm").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/search/page/' + (++document.getElementById("currentPageNext").value) +
        '?searchQuery=' + formData.get("searchQuery")),
        function (data) {
            fillUsersList(data);
        })
    document.getElementById("currentPagePrev").value++;
    checkNexPrevButtons();
    setActivePageNext(document.getElementById("currentPageNext").value);
});

$("#previousForm").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/search/page/' + (--document.getElementById("currentPagePrev").value) +
        '?searchQuery=' + formData.get("searchQuery")),
        function (data) {
            fillUsersList(data);
        })
    document.getElementById("currentPageNext").value--;
    checkNexPrevButtons();
    setActivePagePrev(document.getElementById("currentPagePrev").value);
})

function fillUsersList(data) {
    document.getElementById("list").remove();
    let list = document.createElement("div");
    list.className = 'list-group text-center';
    list.id = 'list';
    document.getElementById("main").appendChild(list);
    ($.map(data, function (val, i) {
        let href = document.createElement("a");
        if (val.type === "group") {
            href.href = ctx + '/groups/' + val.id;
            href.className = 'list-group-item list-group-item-action';
            href.text = 'Group: ' + val.name
            list.appendChild(href);
        } else if (val.type === "user") {
            href.href = ctx + '/users/' + val.id;
            href.className = 'list-group-item list-group-item-action';
            href.text = 'User: ' + val.surname + ' ' + val.name + ' ' + val.midname;
            list.appendChild(href);
        }
    }));
}

function checkNexPrevButtons() {
    if (document.getElementById("currentPagePrev").value == document.getElementById("numberOfPages").value) {
        document.getElementById("nextButton").setAttribute("hidden", "hidden");
    } else {
        document.getElementById("nextButton").removeAttribute("hidden");
    }
    if (document.getElementById("currentPagePrev").value == 1) {
        document.getElementById("previousButton").setAttribute("hidden", "hidden");
    } else {
        document.getElementById("previousButton").removeAttribute("hidden");
    }
}

function setActivePage(currentPage) {
    document.getElementById("page" + document.getElementById("currentPageNext").value).className = "page-item";
    document.getElementById("page" + currentPage).className = "page-item active";
    document.getElementById("currentPageNext").value = currentPage;
    document.getElementById("currentPagePrev").value = currentPage;
}

function setActivePageNext(currentPage) {
    document.getElementById("page" + (--currentPage)).className = "page-item";
    document.getElementById("page" + (++currentPage)).className = "page-item active";
}

function setActivePagePrev(currentPage) {
    document.getElementById("page" + currentPage).className = "page-item active";
    document.getElementById("page" + (++currentPage)).className = "page-item";
}