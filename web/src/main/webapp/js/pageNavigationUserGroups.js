$(".paging").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/users/' + formData.get("id") + '/user-groups/page/' + formData.get("currentPage")),
        function (data) {
            fillGroupsList(data);
        })
    setActivePage(formData.get("currentPage"));
    checkNexPrevButtons();
});

$("#nextForm").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/users/' + formData.get("id") + '/user-groups/page/' + (++document.getElementById("currentPageNext").value)),
        function (data) {
            fillGroupsList(data);
        })
    document.getElementById("currentPagePrev").value++;
    checkNexPrevButtons();
    setActivePageNext(document.getElementById("currentPageNext").value);
});

$("#previousForm").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/users/' + formData.get("id") + '/user-groups/page/' + (--document.getElementById("currentPagePrev").value)),
        function (data) {
            fillGroupsList(data);
        })
    document.getElementById("currentPageNext").value--;
    checkNexPrevButtons();
    setActivePagePrev(document.getElementById("currentPagePrev").value);
})

function fillGroupsList(data) {
    document.getElementById("list").remove();
    let list = document.createElement("div");
    list.className = 'list-group text-center';
    list.id = 'list';
    document.getElementById("main").appendChild(list);
    ($.map(data, function (val, i) {
        let href = document.createElement("a");
        href.href = ctx + '/groups/' + val.id;
        href.className = 'list-group-item list-group-item-action';
        href.text = val.name
        list.appendChild(href);
    }));
}

function checkNexPrevButtons() {
    if (document.getElementById("currentPagePrev").value == document.getElementById("numberOfPages").value) {
        document.getElementById("nextButton").setAttribute("hidden", "hidden");
    } else {
        document.getElementById("nextButton").removeAttribute("hidden");
    }
    if (document.getElementById("currentPagePrev").value == 1) {
        document.getElementById("previousButton").setAttribute("hidden", "hidden");
    } else {
        document.getElementById("previousButton").removeAttribute("hidden");
    }
}

function setActivePage(currentPage) {
    document.getElementById("page" + document.getElementById("currentPageNext").value).className = "page-item";
    document.getElementById("page" + currentPage).className = "page-item active";
    document.getElementById("currentPageNext").value = currentPage;
    document.getElementById("currentPagePrev").value = currentPage;
}

function setActivePageNext(currentPage) {
    document.getElementById("page" + (--currentPage)).className = "page-item";
    document.getElementById("page" + (++currentPage)).className = "page-item active";
}

function setActivePagePrev(currentPage) {
    document.getElementById("page" + currentPage).className = "page-item active";
    document.getElementById("page" + (++currentPage)).className = "page-item";
}