var stompClient = null;

let chatId = document.getElementById('chatId').value
let senderId = document.getElementById('senderId').value
let recipientId = document.getElementById('recipientId').value
let senderName = document.getElementById('senderName').value
let recipientName = document.getElementById('recipientName').value


function connect() {
    let socket = new SockJS(location.protocol + '//' + location.host + ctx + '/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);
}

function onConnected() {
    stompClient.subscribe(
        "/user/" + senderId + "/messages",
        onMessageReceived
    );
}

function onError() {
    console.log('service unavailable');
}

function sendMessage(event) {
    let messageContent = $("#message").val();
    if (messageContent.trim() !== "") {
        const message = {
            chatId: chatId,
            senderId: senderId,
            recipientId: recipientId,
            content: messageContent,
            timestamp: new Date(),
        };
        stompClient.send("/app/chat", {}, JSON.stringify(message));
        let messages = document.getElementById('list');
        let newMessage = document.createElement('div')
        let p = document.createElement('p')
        p.className = 'text-break'
        let spanTime = document.createElement('span')
        spanTime.className = 'fw-light fs-6 fst-italic'
        spanTime.innerText = 'Time: ' + message.timestamp.getHours() + ':' + message.timestamp.getMinutes() + ' '
        let spanSenderName = document.createElement('span')
        spanSenderName.className = 'badge bg-secondary fs-5'
        spanSenderName.innerText = senderName
        let spanMessage = document.createElement('span')
        spanMessage.innerText = message.content
        p.appendChild(spanTime)
        p.appendChild(spanSenderName)
        p.appendChild(spanMessage)
        newMessage.appendChild(p)
        messages.appendChild(newMessage)
        $("#message").val('')
        event.preventDefault();
        scrollBottom()
    }
}

function onMessageReceived(payload) {
    let message = JSON.parse(payload.body);
    let messages = document.getElementById('list');
    let newMessage = document.createElement('div')
    let p = document.createElement('p')
    p.className = 'text-break'
    let spanTime = document.createElement('span')
    spanTime.className = 'fw-light fs-6 fst-italic'
    let currentDate = new Date()
    spanTime.innerText = 'Time: ' + currentDate.getHours() + ':' + currentDate.getMinutes() + ' '
    let spanSenderName = document.createElement('span')
    spanSenderName.className = 'badge bg-secondary fs-5'
    if (senderName === message.sender.name) {
        spanSenderName.innerText = senderName
    } else {
        spanSenderName.innerText = recipientName
    }
    let spanMessage = document.createElement('span')
    spanMessage.innerText = message.content
    p.appendChild(spanTime)
    p.appendChild(spanSenderName)
    p.appendChild(spanMessage)
    newMessage.appendChild(p)
    messages.appendChild(newMessage)
    scrollBottom()
}

function scrollBottom() {
    $('#scroll').scrollTop($('#scroll')[0].scrollHeight);
}

$(document).ready(function () {
    connect();
    document.querySelector("#messagebox").addEventListener('submit', sendMessage, true)
    window.scrollTo(0, 100);
    scrollBottom()
});