$(function () {
    $("#search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: ctx + "/search-autocomplete",
                data: {
                    filter: request.term
                },
                success: function (data) {
                    response($.map(data, function (val, i) {
                        let res
                        let idType;
                        if (val.type === "group") {
                            idType = "/groups/" + val.id
                            res = val.name
                        } else if (val.type === "user") {
                            idType = "/users/" + val.id
                            res = val.surname + " " + val.name + " " + val.midname;
                        }
                        return {value: idType, label: res}
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            location.href = ctx + ui.item.value
            return false;
        }
    });
});