$(".paging").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/users/' + formData.get("id") + '/chats/page/' + formData.get("currentPage")),
        function (data) {
            fillChatsList(data);
        })
    setActivePage(formData.get("currentPage"));
    checkNexPrevButtons();
});

$("#nextForm").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/users/' + formData.get("id") + '/chats/page/' + (++document.getElementById("currentPageNext").value)),
        function (data) {
            fillChatsList(data);
        })
    document.getElementById("currentPagePrev").value++;
    checkNexPrevButtons();
    setActivePageNext(document.getElementById("currentPageNext").value);
});

$("#previousForm").submit(function (e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    $.get((ctx + '/users/' + formData.get("id") + '/chats/page/' + (--document.getElementById("currentPagePrev").value)),
        function (data) {
            fillChatsList(data);
        })
    document.getElementById("currentPageNext").value--;
    checkNexPrevButtons();
    setActivePagePrev(document.getElementById("currentPagePrev").value);
})

function fillChatsList(data) {
    document.getElementById("list").remove();
    let list = document.createElement("div");
    list.className = 'list-group text-center';
    list.id = 'list';
    document.getElementById("main").appendChild(list);
    ($.map(data, function (val, i) {
        let href = document.createElement("a");
        let id = document.getElementById("id").value;
        if (val.sender.id == id) {
            href.href = ctx + '/users/' + id + '/chat?to=' + val.recipient.id;
            href.text = 'Chat with ' + val.recipient.name
        } else {
            href.href = ctx + '/users/' + id + '/chat?to=' + val.sender.id;
            href.text = 'Chat with ' + val.sender.name
        }
        href.className = 'list-group-item list-group-item-action';
        list.appendChild(href);
    }));
}

function checkNexPrevButtons() {
    if (document.getElementById("currentPagePrev").value == document.getElementById("numberOfPages").value) {
        document.getElementById("nextButton").setAttribute("hidden", "hidden");
    } else {
        document.getElementById("nextButton").removeAttribute("hidden");
    }
    if (document.getElementById("currentPagePrev").value == 1) {
        document.getElementById("previousButton").setAttribute("hidden", "hidden");
    } else {
        document.getElementById("previousButton").removeAttribute("hidden");
    }
}

function setActivePage(currentPage) {
    document.getElementById("page" + document.getElementById("currentPageNext").value).className = "page-item";
    document.getElementById("page" + currentPage).className = "page-item active";
    document.getElementById("currentPageNext").value = currentPage;
    document.getElementById("currentPagePrev").value = currentPage;
}

function setActivePageNext(currentPage) {
    document.getElementById("page" + (--currentPage)).className = "page-item";
    document.getElementById("page" + (++currentPage)).className = "page-item active";
}

function setActivePagePrev(currentPage) {
    document.getElementById("page" + currentPage).className = "page-item active";
    document.getElementById("page" + (++currentPage)).className = "page-item";
}